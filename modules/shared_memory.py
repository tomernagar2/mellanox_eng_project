from generals import *
from globals import *
from .mng_commands import *
from configuration import shared_memory_config


class SharedMemory(GeneralUnit):
    """
        RxUnit object responsible on Rx flow.
    """
    def __init__(self, name, id, storage, config_file=shared_memory_config.SharedMemoryConfig()):
        """
        init new Rx unit object
        """

        handler = config_file.get_handler(sm_default_handler)
        init_handler = config_file.get_init_handler(sm_default_init_handler)
        finish_handler = config_file.get_final_handler(sm_final_handler)
        super(SharedMemory, self).__init__(target=handler, target_init=init_handler, target_final=finish_handler, name=name, one_time=False, config_file=config_file, args=(self, name, id, storage,))


def sm_default_init_handler(self, name, id, local_storage):
    self.printer(HIGH, "I am shared memory #" + str(id) + " with name: " + str(name) + " started!")
    local_storage["QPS"] = {} ## init Qp dir.

    local_storage["PciFabricManager"] = FabricManager(self, local_storage[PCIBusName])
    local_storage["PciFabricManager"]\
        .add_handler(OpenQpCommand.__name__, FabricManager
                     .get_handler_wrapper(sm_new_qp_handler, self, name, id, local_storage))

    init_complete(name)


def sm_default_handler(self, name, id, local_storage):
    pci_fabric_manager = local_storage["PciFabricManager"]
    pci_fabric_manager.run()




def sm_final_handler(self, name, id, local_storage):
    pass


def sm_new_qp_handler(self, name, id, local_storage, packet):
    pci_fabric_manager = local_storage["PciFabricManager"]

    local_storage["QPS"][packet.qp.get_qp_id()] = packet.qp
    self.printer(MEDIUM, "New QP opened!\n" + str(packet.qp))
    response = OpenQpCompleteCommand.get_res_to_req(packet)
    pci_fabric_manager.send(response, dest_name="ManagmentUnit")
    self.printer(HIGH, "New QP Response sent")

