from generals import *
from globals import *
from configuration import tx_config
from .arq_commands_packets import *


class TxUnit(GeneralUnit):
    """
        RxUnit object responsible on Tx flow.
    """
    def __init__(self, name, id, storage, config_file=tx_config.TxConfig()):
        """
        init new Tx unit object
        """

        handler = config_file.get_handler(tx_default_handler)
        init_handler = config_file.get_init_handler(tx_default_init_handler)
        finish_handler = config_file.get_final_handler(tx_default_final_handler)
        super(TxUnit, self).__init__(target=handler, target_init=init_handler, target_final=finish_handler, name=name,
                                      one_time=False, config_file=config_file, args=(self, name, id, storage,))


def tx_default_init_handler(self, name, id, local_storage):
    self.printer(HIGH, "I am tx #" + str(id) + " with name: " + str(name) + " started!")

    # for entry in Nic.MemberReqList:
    #     if entry.ItemName == 'Rx':
    #         for keyw in entry.req_kwords:
    #             if storage.get(keyw, None) is None:
    #                 dut_error(7, "RxUnit: "+name+" Init fail - Unit requested param: "
    #                                              ""+keyw+": while storage not has it!")
    local_storage["InternalFabricManager"] = FabricManager(self, local_storage[InternalBusName])
    local_storage["FabricManager"] = FabricManager(self, local_storage[FabricName])
    local_storage["Req_sent"] = False
    local_storage["statistics"] = {'packet_counter': 0, 'sid_list': {}, 'total_size': 0}
    init_complete(name)


def tx_default_handler(self, name, id, local_storage):
    internal_fabric = local_storage["InternalFabricManager"]
    fabric = local_storage["FabricManager"]
    if not local_storage["Req_sent"]:
        local_storage["Req_sent"] = True
        new_req = ArqCommandReqToSend(internal_fabric.my_address)
        internal_fabric.send(new_req, dest_name='Arq')

    new_control_command = internal_fabric.get()
    local_storage["Req_sent"] = False
    if new_control_command is None or not new_control_command.contain_data():
        return
    packet = new_control_command.get_pyload()
    dest_host = new_control_command.get_target()
    self.printer(MEDIUM, "got new packet dest to: " + str(dest_host) + "\n" + str(packet))
    tx_update_statistics(self, local_storage, packet)
    fabric.send(packet, dest_host)


def tx_default_final_handler(self, name, id, local_storage):
    statistics = local_storage['statistics']
    packet_counter = statistics['packet_counter']
    sid_list = statistics['sid_list']
    num_of_sids = len(sid_list)
    total_size = statistics['total_size']
    string = "\n"
    string += "---------------"+name+"---------------\n"
    string += "------------Simulator statistics-----------\n"
    string += "-------------------------------------------\n"
    string += "Total num of packets: " +str(packet_counter) + "\n"
    string += "Num of retransmits: " + str(packet_counter-num_of_sids) + "\n"
    string += "Avg packet size: " + str(total_size/num_of_sids) + "\n"
    string += "Throughput: " + str((num_of_sids * 100)/packet_counter) + "%\n"
    string += "-------------------------------------------\n"
    self.printer(STATISTICS, string)


def tx_update_statistics(self, local_storage, packet):
    statistics = local_storage['statistics']
    statistics['packet_counter'] += 1
    packet_sid = str(packet.get_sid())
    sid_counter = statistics['sid_list'].get(packet_sid, 0)
    statistics['sid_list'][packet_sid] = sid_counter+1
    if sid_counter == 0:
        statistics['total_size'] += packet.size

