from generals import *
from globals import *
from modules.rx_unit import *
from modules.tx_unit import *
from modules.mng_unit import *
from modules.arq_unit import *
from modules.shared_memory import *
from modules.propagation_fabric import *
from configuration import nic_config
config = nic_config.NicConfig


class Nic(GeneralThread):
    """

        nic object, holds and manipulate all chip units
    """
    # nic_address = None
    # fabric = None
    # simulation_fabric = None

    def __init__(self, nic_name, nic_id, fabric, simulation_fabric, config_file=nic_config.NicConfig()):
        """
        init new nic object
        """
        handler = config_file.get_handler(nic_default_handler)
        init_handler = config_file.get_init_handler(nic_default_init_handler)
        finish_handler = config_file.get_final_handler(nic_default_final_handler)
        ## TODO: convert to new metodology - enter fabric & simulation fabric **args from Simulation.
        super(Nic, self).__init__(target=handler, target_init=init_handler, target_final=finish_handler, name=nic_name,
                                  one_time=False, args=(self, nic_name, nic_id, {
                                      FabricName: fabric.get_wrapper_for(address=nic_id),
                                      SimulationFabricName: simulation_fabric.get_wrapper_for(address=nic_id)
                                  },))


# default List for Nic
Nic.UnitsToInitList = [
    UnitInitReqEntry('Rx', RxUnit, [NicId, InternalBusName, ExternalBusName, FabricName]),
    UnitInitReqEntry('Tx', TxUnit, [NicId, InternalBusName, ExternalBusName, FabricName]),
    UnitInitReqEntry('Arq', ArqUnit, [NicId, InternalBusName, ExternalBusName]),
    UnitInitReqEntry('SharedMemory', SharedMemory, [NicId, ExternalBusName, PCIBusName]),
    UnitInitReqEntry('ManagmentUnit', MngUnit, [NicId, PCIBusName, SimulationFabricName])
]

Nic.InternalBusList = [
    BusEntryManager(bus_name=InternalBusName, class_name=CommunicationQueue),
    BusEntryManager(bus_name=ExternalBusName, class_name=CommunicationQueue),
    BusEntryManager(bus_name=PCIBusName, class_name=CommunicationQueue)
]


def nic_default_init_handler(self, nic_name, nic_id, local_storage):
    self.printer(HIGH, "I am nic #" + str(nic_id) + " with name: " + str(nic_name) + " started!")
    self.InternalBusList = copy.deepcopy(self.InternalBusList)
    self.UnitsToInitList = copy.deepcopy(self.UnitsToInitList)

    local_storage[Nic] = self
    local_storage[NicName] = nic_name
    local_storage[NicId] = nic_id

    #count num of entrys for each fabric
    for fab in self.InternalBusList:
        for member in self.UnitsToInitList:
            if fab.bus_name in member.req_kwords:
                fab.add_member(member.ItemName)

    #Copy requested arguments to members.
    for member in self.UnitsToInitList:
        member.local_storage = {}
        for arg in local_storage:
            if arg in member.req_kwords:
                member.local_storage[arg] = local_storage[arg]

    for fab in self.InternalBusList:
        local_storage[fab.bus_name] = fab.create_bus()
        for member in self.UnitsToInitList:
            if fab.bus_name in member.req_kwords:
                member.local_storage[fab.bus_name] = local_storage[fab.bus_name].get_wrapper_for(name=member.ItemName)

    member_id = 0
    for member in self.UnitsToInitList:
        member_name = nic_name + member.ItemName
        add_to_init_list(member_name)
        member.Class(member_name, member_id, member.local_storage).start()
        member_id += 1

    init_complete(nic_name)


def nic_default_handler(self, nic_name, nic_id, local_storage):
    time.sleep(3)


def nic_default_final_handler(self, nic_name, nic_id, local_storage):
    pass
