from generals import *
from globals import *
from configuration import rx_config
from .arq_commands_packets import *


class RxUnit(GeneralUnit):
    """
        RxUnit object responsible on Rx flow.
    """
    def __init__(self, name, id, storage, config_file=rx_config.RxConfig()):
        """
        init new Rx unit object
        """
        handler = config_file.get_handler(rx_default_handler)
        init_handler = config_file.get_init_handler(rx_default_init_handler)
        finish_handler = config_file.get_final_handler(rx_default_final_handler)
        super(RxUnit, self).__init__(target=handler, target_init=init_handler, target_final=finish_handler, name=name, one_time=False, config_file=config_file, args=(self, name, id, storage,))


def rx_default_init_handler(self, name, id, local_storage):
    self.printer(HIGH, "I am rx #" + str(id) + " with name: " + str(name) + " started!")

    local_storage["InternalFabricManager"] = FabricManager(self, local_storage[InternalBusName])
    local_storage["FabricManager"] = FabricManager(self, local_storage[FabricName])
    init_complete(name)


def rx_default_handler(self, name, id, local_storage):
    fabric_manager = local_storage["FabricManager"]
    internal_fabric_manager = local_storage["InternalFabricManager"]

    new_packet = fabric_manager.get()
    if new_packet is None:
        return

    self.printer(MEDIUM, "New packet arrived to Rx! \n" + str(new_packet))
    wrapped_packet = ArqCommandRxUpdate(internal_fabric_manager.my_address, new_packet)
    internal_fabric_manager.send(wrapped_packet, dest_name='Arq')


def rx_default_final_handler(self, name, id, local_storage):
    pass
