import datetime
from .communication_queue import *
from sim_pack.sim_globals import *
from configuration import propagation_fabric_config
config = propagation_fabric_config.FabricConfig


class PropagationQueueEntry(object):
    '''
    basic extention to fubric of type Propagation
    '''
    valid = False
    Queue = None
    pyload = None

    def __init__(self, channel_propagation=None):
        self.completion_time = None
        if channel_propagation is not None:
            self.channel_propagation = channel_propagation
        else:
            self.channel_propagation = config.Propagation

        self.queue = Queue()

    # internals
    def _is_valid(self):
        return self.valid

    def _set_valid(self):
        self.valid = True

    def _reset_valid(self):
        self.valid = False

    def _reset_completion_time(self):
        self.completion_time = 0

    def _set(self, pyload, pyload_size):
        self.pyload = pyload
        self.completion_time = CURRENT_TIME() + datetime.timedelta(microseconds=pyload_size * self.channel_propagation)
        messagef(HIGH, "_set, CURRENT_TIME(): " + str(CURRENT_TIME()) + " dest in: " + str(self.completion_time))
        self._is_complete()

    def _get_completion(self, block=True, timeout=None):
        self.pyload = None
        self._reset_valid()
        self._reset_completion_time()
        self._update_status(block, timeout)

    def _is_complete(self, update_vld=True):
        result = self._has_pyload() and CURRENT_TIME() >= self.completion_time
        if update_vld and result and not self._is_valid():
            self._set_valid()
        return result

    def _has_pyload(self):
        return self.pyload is not None

    def _update_status(self, block=True, timeout=None):
        if self.pyload is None and not self.queue.empty():
            (pyload, pyload_size) = self.queue.get(block, timeout)
            self._set(pyload, pyload_size)
        self._is_complete(update_vld=True)

    # end of internals

    def get(self, block=True, timeout=None):
        assert self._is_valid(), "PropogationEntry {0!r} not valid! can't get()".format(self)
        result = self.pyload
        self._get_completion(block, timeout)
        return result

    def empty(self, block=True, timeout=None):
        self._update_status(block, timeout)
        return not self._is_valid()

    def put(self, pyload, block=True, timeout=None):
        assert self.queue is not None, "PropogationQueu {0!r} is None can't put()".format(self)
        assert hasattr(pyload,
                       'size') is not None, "PropogationQueu {0!r} got packet with out ettribute size - Requered! ".format(
            self)
        propagation_packet = (pyload, pyload.size)
        return self.queue.put(propagation_packet, block, timeout)

    def qsize(self):
        return (self._is_valid() * 1) + self.queue.qsize()

    def full(self):
        assert self.queue is not None, "PropogationQueu {0!r} is None can't full()".format(self)
        return self.queue.full()

    def get_nowait(self):
        return self.get(block=False)

    def put_nowait(self, pyload):
        return self.put(pyload, block=False)

    def close(self):
        assert self.queue is not None, "PropogationQueu {0!r} is None can't close()".format(self)
        self.queue.close()

    def join_thread(self):
        assert self.queue is not None, "PropogationQueu {0!r} is None can't join_thread()".format(self)
        self.queue.join_thread()

    def cancel_join_thread(self):
        assert self.queue is not None, "PropogationQueu {0!r} is None can't cancel_join_thread()".format(self)
        self.queue.cancel_join_thread()


class PropagationQueue(CommunicationQueue):
    def __init__(self, num_of_types=1, member_list=None):
        super(PropagationQueue, self).__init__(num_of_types=num_of_types, custom_queue=PropagationQueueEntry, member_list=member_list)
