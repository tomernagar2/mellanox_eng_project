from generals import *
from globals import *
from modules.mng_commands import *
from configuration import simulation_controller_config


class SimulationController(GeneralUnit):
    """
        SimulationController object responsible on Rx flow.
    """
    def __init__(self, name, id, sim_fabric, config_file=simulation_controller_config.SimulationControllerConfig):
        """
        init new SimulationController object
        """

        handler = sim_cont_default_handler
        init_handler = sim_cont_default_init_handler
        if config_file.Custom_handler is not None:
            handler = config_file.Custom_handler
        if config_file.Custom_init_handler is not None:
            init_handler = config_file.Custom_init_handler
        if config_file.DisableInitFunc:
            init_handler = None
        storage = {SimulationFabricName: sim_fabric}
        super(SimulationController, self).__init__(target=handler, target_init=init_handler, name=name, one_time=False, config_file=config_file, args=(self, name, id, storage,))


def sim_cont_default_init_handler(self, name, id, local_storage):
    self.printer(HIGH, "I am simulation_controller #" + str(id) + " with name: " + str(name) + " started!")
    local_storage["Qps"] = {
        'Open': {},
        'OnInit': {},
        'Closing': {}
    }
    local_storage["PendingReq"] = []
    init_complete(name)


def sim_cont_default_handler(self, name, id, local_storage):
    update = sim_cont_get_update(self, name, id, local_storage)
    if update is None:
        pass
    elif isinstance(update, RequestCommand):
        sim_cont_req_handler(self, name, id, local_storage, update)
    elif isinstance(update, ResponseCommand):
        sim_cont_res_handler(self, name, id, local_storage, update)
    elif isinstance(update, UpdateCommand):
        sim_cont_update_handler(self, name, id, local_storage, update)
    else:
        dut_error(1, "Got update from unknown instance: " + str(type(update)) + ", info: " + str(update), name)
    internal_generator(self, name, id, local_storage)
    if sim_cont_has_waiting_req(self, name, id, local_storage):
        waiting_reqs = sim_cont_get_waiting_reqs(self, name, id, local_storage)
        for req in waiting_reqs:
            sim_cont_send_req(self, name, id, local_storage, req)

    add_random_wqe(self, name, id, local_storage)


def sim_cont_get_update(self, name, id, local_storage):
    result = None
    if not local_storage[SimulationFabricName].empty(id):
        result = local_storage[SimulationFabricName].get(id)
        self.printer(HIGH, "sim_cont_get_update - New PacketArrived")
    return result


def sim_cont_req_handler(self, name, id, local_storage, update):
    #self.printer(HIGH, "sim_cont_new_req_handler - called")
    pass


def sim_cont_res_handler(self, name, id, local_storage, update):
    self.printer(HIGH, "sim_cont_new_res_handler got new response! \n" + str(update))
    qp_hash = local_storage["Qps"]['OnInit'][update.source][update.get_sid()]

    local_storage["Qps"]['Open'][update.source] = {
        qp_hash: local_storage["Qps"]['OnInit'][update.source][qp_hash]
    }
    local_storage["Qps"]['OnInit'][update.source][qp_hash] = None
    local_storage["Qps"]['OnInit'][update.source][update.get_sid()] = None

    self.printer(HIGH, "OpenQP updated succesfuly: \n" + str(local_storage["Qps"]['Open']))


def sim_cont_update_handler(self, name, id, local_storage, update):
    #self.printer(HIGH, "sim_cont_update_handler - called")
    pass


def sim_cont_get_waiting_reqs(self, name, id, local_storage):
    result = local_storage["PendingReq"]

    self.printer(HIGH, "sim_cont_get_waiting_reqs - called")
    self.printer(HIGH, "PendingReqList = " + str(result))

    local_storage["PendingReq"] = []
    return result


def sim_cont_has_waiting_req(self, name, id, local_storage):
    return len(local_storage["PendingReq"]) > 0


def sim_cont_add_req(self, name, id, local_storage, req):
    self.printer(HIGH, "sim_cont add req to PendingReq:\n" + str(req))
    local_storage["PendingReq"].append(req)


def sim_cont_send_req(self, name, id, local_storage, req):
    self.printer(HIGH, "sim_cont_send_req - sending req with sid: " + str(req.get_sid()) + " to target: " + str(req.target))
    local_storage[SimulationFabricName].put(req, req.target)


def qp_hash_function(requester_lin, responder_lin):
    return str(requester_lin)+"=>"+str(responder_lin)


def internal_generator(self, name, id, local_storage):
    no_open_qp = len(local_storage["Qps"]['Open']) == 0 and \
                 len(local_storage["Qps"]['OnInit']) == 0

    if no_open_qp:
        requster, responder = gen_source_target()
        req_res_hash = qp_hash_function(requster, responder)
        req_qp, res_qp = gen_qp(requster, responder, local_storage)

        req_command = OpenQpCommand(req_qp, target=requster, source=id)
        res_command = OpenQpCommand(res_qp, target=responder, source=id)

        local_storage["Qps"]['OnInit'][requster] = {
            req_res_hash: req_qp,
            req_command.get_sid(): req_res_hash
        }

        local_storage["Qps"]['OnInit'][responder] = {
            req_res_hash: res_qp,
            res_command.get_sid(): req_res_hash
        }

        sim_cont_add_req(self, name, id, local_storage, req_command)
        sim_cont_add_req(self, name, id, local_storage, res_command)


def gen_source_target():
    #  TODO: return source, destination to open Qp.
    return 1, 2


#  TODO: need to auto generate it.
def gen_qp(requster, responder, local_storage):
    from .basic_qp import BasicQP
    req_qp_id = 3 ##TODO: auto generate it.
    res_qp_id = 4
    req_qp = BasicQP(req_qp_id, "data", "context")
    req_qp.init_wq(wq_id=0, qp_id=req_qp_id)
    req_qp.init_cq(cq_id=0, qp_id=req_qp_id)

    res_qp = BasicQP(res_qp_id, "data", "context")
    res_qp.init_wq(wq_id=0, qp_id=res_qp_id)
    res_qp.init_cq(cq_id=0, qp_id=res_qp_id)

    return req_qp, res_qp


def add_random_wqe(self, name, id, local_storage):
    qp_list = local_storage["Qps"]['Open']
    if len(qp_list) == 0:
        return
    random_qp = get_random_qp(self, name, id, local_storage)


def get_random_qp(self, name, id, local_storage):
    ##TODO: Need continue here.
    qp_items = list(local_storage["Qps"]['Open'].items())
    selected_qp_num = random.randint(0, len(qp_items)-1)
    selected_qp = qp_items[selected_qp_num]
    self.printer(DEBUG, "get_random_qp:\nqp_items:" + str(qp_items) +
                 "\nselected_qp_num: " + str(selected_qp_num) +
                 "\nselected_qp: " + str(selected_qp))
