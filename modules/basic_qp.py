from generals import *


class BasicWQ(CopyKwArgs, WQ):
    def __init__(self, **kwargs):
        CopyKwArgs.__init__(self, **kwargs)
        WQ.__init__(self, **kwargs)


class BasicCQ(CopyKwArgs, CQ):
    def __init__(self, **kwargs):
        CopyKwArgs.__init__(self, **kwargs)
        CQ.__init__(self, **kwargs)


class BasicWQE(CopyKwArgs, WQE):
    def __init__(self, **kwargs):
        CopyKwArgs.__init__(self, **kwargs)
        WQE.__init__(self)


class BasicCQE(CopyKwArgs, CQE):
    def __init__(self, **kwargs):
        CopyKwArgs.__init__(self, **kwargs)
        CQE.__init__(self)


class BasicQP(QP):
    _Context = None
    _Data = None

    def __init__(self, qp_id, QpData, QpContext):
        super(BasicQP, self).__init__(qp_id)
        self._Data = QpData
        self._Context = QpContext

    def init_wq(self, **kwargs):
        new_wq = BasicWQ(**kwargs)
        self.add_wq(new_wq, '0')

    def init_cq(self, **kwargs):
        new_cq = BasicCQ(**kwargs)
        self.add_cq(new_cq, '0')

    def __str__(self):
        result = super(BasicQP, self).__str__()
        result += "{0:_^63}\n".format('')
        result += "|{0:^61}|\n".format("Context")
        result += "{0:_^63}\n".format('')
        result += str(self._Context) + "\n"
        result += "{0:_^63}\n".format('')
        result += "{0:_^63}\n".format('')
        result += "|{0:^61}|\n".format("Data")
        result += "{0:_^63}\n".format('')
        result += str(self._Data) + "\n"
        result += "{0:_^63}\n".format('')
        return result
    # def add_wq(self, wq):
    #     assert isinstance(wq, WQ), "QP {0!r} got wq with type {1!r} " \
    #                                         "no instance of WQ".format(self, type(wq))
    #     if wq.


