from configuration.qp_config import QPConfig


# exceptions decelerations
class QpNotExists(Exception):
    pass


class QpAlreadyExists(Exception):
    pass


# objects
class OldQP(object):
    """
    Holds process Qps list and data.
    allow create new Qps.

    """

    # global part
    Global_QpRequesterList = []
    Global_QpResponderList = []
    Global_QpInUse = []

    globalQpToConfig = None
    # format: {host1_num : {role, qp_num}, host2_num : {role, qp_num}}
    # when role = 'requester' / 'responder'

    globalQpConfigComplete = False

    # local part
    fields = {
        'qpInitial': False,
        'localQpNumber': None,  # format: <number>   TODO: add gvmi support.
        'remoteQpNumber': None,  # format: ({'host' : host1_num, 'qp' : Qp_num}, {'host' : host2_num, 'qp' : Qp_num})
        'hostRole': None,  # format: 'requester' / 'responder'
    }

    def __init__(self):
        OldQP.config()
        self.fields = {'qpInitial': False}

    @staticmethod
    def config():
        if OldQP.globalQpToConfig is None:
            OldQP.globalQpToConfig = QPConfig().QP_CONFIG_LIST

    @staticmethod
    def new_qp(qp_config):
        """

        :param qp_config:
        :return:
        """
        # verifies that
        if qp_config['localQpNumber'] in OldQP.Global_QpInUse:
            raise QpNotExists("qp#%d already in use!" % qp_config['localQpNumber'])

        new = OldQP()
        new.fields['remoteQpNumber'] = qp_config['remoteQpNumber']
        new.fields['localQpNumber'] = qp_config['localQpNumber']
        new.fields['hostRole'] = qp_config['hostRole']

        if new.fields['hostRole'] == 'requester':
            OldQP.Global_QpRequesterList.append(new)
            new.fields['qpInitial'] = True

        elif new.fields['hostRole'] == 'responder':
            OldQP.Global_QpResponderList.append(new)
            new.fields['qpInitial'] = True

        print("New QP created:")
        new.print_qp()
        print("------------\n")
        return new

    @staticmethod
    def get_requester_qp(qp_id):
        """
        return Qp from QprequesterList with "localQpNumber == qp_id" if exist, else raise QP_NOT_EXISTS exception.

        :param qp_id:
        :return:
            QP object.
        """

        try:
            return [qp for qp in OldQP.Global_QpRequesterList if qp.fields['localQpNumber'] == qp_id][0]  # will fail if
        except Exception:
            raise QpNotExists("Q#%d not exist in requesterQP list" % qp_id)

    @staticmethod
    def get_responder_qp(qp_id):
        """
        return Qp from QpResponderList with "localQpNumber == qp_id" if exist, else raise QP_NOT_EXISTS exception.
        :param qp_id:
        :return:
        QP object.
        """
        try:
            return [qp for qp in OldQP.Global_QpResponderList if qp.fields['localQpNumber'] == qp_id][0]  # will fail if
        except Exception:
            raise QpNotExists("Q#%d not exist in responderQP list" % qp_id)

    @staticmethod
    def init_qps_for_host(local_host_address):
        OldQP.config()  # load QPS configuration.
        host_relevant_qps_list = [conf for conf in OldQP.globalQpToConfig if local_host_address in conf.keys()]
        for conf in host_relevant_qps_list:
            # conf format: {0 : {'rule' : 'requester','qp_number' : 0}, 1 : {'rule': 'responder', 'qp_number' : 1}}
            local_qp_number = conf[local_host_address]['qp_number']
            host_rule = conf[local_host_address]['rule']
            remote_qp_list = [{
                'host': remote_host_address,
                'qp': conf[remote_host_address]['qp_number'],
            } for remote_host_address in conf.keys() if remote_host_address != local_host_address
            ]
            OldQP.new_qp({
                'localQpNumber': local_qp_number,
                'remoteQpNumber': remote_qp_list,
                'hostRole': host_rule,
            })
        print("Global_QpRequesterList:", OldQP.Global_QpRequesterList, "Global_QpResponderList", OldQP.Global_QpResponderList)
        OldQP.print_global_qp_status(local_host_address)

    @staticmethod
    def print_global_qp_status(host_address):
        print("--------------------")
        print("qp status at host#", host_address)
        print("--------------------")
        i = 0
        for a in OldQP.Global_QpRequesterList:
            #printf("%d. " % i)
            i += 1
            a.print_qp()
        for b in OldQP.Global_QpResponderList:
            #printf("%d. " % i)
            i += 1
            b.print_qp()
        print("-------END----------\n")

    def qp_init_test(self):
        """
        Test that the QP initialed before use, raise assert exception if not.
        """
        assert self.fields['qpInitial'], "QP {0!r} not initialed!\nQP info: {1!r}".format(self, self.fields)

    def print_qp(self):
        self.qp_init_test()
        print("QP info:", self, "\n", self.fields, "\n")



