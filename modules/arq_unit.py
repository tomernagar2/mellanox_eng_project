from generals import *
from globals import *
from configuration import arq_commands_packet_config as command_config
from configuration import arq_config
from .packets import *
from .arq_commands_packets import *


class ArqUnit(GeneralUnit):
    """
        RxUnit object responsible on Arq flow.
    """
    def __init__(self, name, id, storage, config_file=arq_config.ArqConfig()):
        """
        init new Rx unit object
        """
        handler = config_file.get_handler(arq_default_handler)
        init_handler = config_file.get_init_handler(arq_default_init_handler)
        finish_handler = config_file.get_final_handler(arq_default_final_handler)
        super(ArqUnit, self).__init__(target=handler, target_init=init_handler, target_final=finish_handler, name=name, one_time=False, config_file=config_file, args=(self, name, id, storage,))


def arq_default_init_handler(self, name, id, local_storage):
    self.printer(HIGH, "I am arq #" + str(id) + " with name: " + str(name) + " started!")

    local_storage["InternalFabricManager"] = FabricManager(self, local_storage[InternalBusName])
    local_storage["waiting_packet_vld"] = False
    local_storage["waiting_packet"] = None
    local_storage["packet_time_stamp"] = CURRENT_TIME()
    local_storage["Time_out"] = arq_config.ArqConfig.ARQ_TIME_OUT
    local_storage["next_psn"] = 0
    local_storage["ack_waiting_list"] = []
    local_storage["ack_pending_vld"] = False
    init_complete(name)


def arq_default_handler(self, name, id, local_storage):

    def time_out():
        time_stamp = CURRENT_TIME()
        packet_time = local_storage["packet_time_stamp"]
        return (time_stamp - packet_time) > local_storage["Time_out"]

    # Stop&Wait #
    internal_fabric_manager = local_storage["InternalFabricManager"]
    new_packet = internal_fabric_manager.get()

    # no request.
    if new_packet is None:
        return

    if not new_packet.packet_type == command_config.ArqCommandsPacketConfig.ArqPacketType:
        dut_error(100, "arq got packet of type != ArqPacketType", name)

    ack_select = gen_random_bool(arq_config.ArqConfig.ACK_PRIO)
    gen_new_packet = gen_random_bool(arq_config.ArqConfig.NEW_PACKET_P) and sim_state[0] == SIM_STATE_RUN
    command_type = new_packet.get_command_type()
    if command_type == command_config.ArqCommandsPacketConfig.ReqToSendCommandType:
        current_time_stamp = CURRENT_TIME()
        ##  first check if there is vld acks and this is ack turn.
        if local_storage["ack_pending_vld"] and (ack_select
                                                 or not local_storage["waiting_packet_vld"]
                                                 or (local_storage["waiting_packet_vld"] and not time_out())):
            poped_ack = local_storage["ack_waiting_list"].pop(0)
            local_storage["ack_pending_vld"] = len(local_storage["ack_waiting_list"]) > 0
            self.printer(MEDIUM, "sending ack! \n" + str(poped_ack.get_pyload()))
            internal_fabric_manager.send(poped_ack, dest_name='Tx')
        elif local_storage["waiting_packet_vld"] and time_out():
            ## retransmiting
            self.printer(MEDIUM, "packet timeout, retransmiting... \n" + str(local_storage["waiting_packet"].get_pyload()))
            internal_fabric_manager.send(local_storage["waiting_packet"], dest_name='Tx')
            local_storage["packet_time_stamp"] = CURRENT_TIME()
        elif not local_storage["waiting_packet_vld"] and gen_new_packet:
            ## Generate new packet
            gened_packet = SAWArqPacket.gen_packet(local_storage["next_psn"], local_storage[NicId],
                                                   packet_max_size=arq_config.ArqConfig.MAX_PACKET_SIZE)
            dest_nic = gen_dest_host(local_storage[NicId])
            local_storage["waiting_packet"] = ArqCommandResToSend(local_storage[NicId], dest_nic, gened_packet)
            local_storage["packet_time_stamp"] = CURRENT_TIME()
            local_storage["waiting_packet_vld"] = True
            local_storage["next_psn"] = (local_storage["next_psn"] + 1) % 2
            self.printer(MEDIUM, "generating new packet, to host: " + str(dest_nic) +"\n" + str(gened_packet))
            internal_fabric_manager.send(local_storage["waiting_packet"], dest_name='Tx')
            raise_objection(name)
        else: # gen empty packet
            dummy_packet = ArqCommandResToSend(local_storage[NicId], 0, None)
            internal_fabric_manager.send(dummy_packet, dest_name='Tx')

    elif command_type == command_config.ArqCommandsPacketConfig.RxUpdateCommandType:
        packet_pyload = new_packet.get_pyload()
        if not isinstance(packet_pyload, SAWArqPacket):
            dut_error(101, "arq got update packet with pyload != SAWArqPacket, type is: "+str(type(packet_pyload)), name)

        source_nic_id = packet_pyload.source_nic_id
        ack = packet_pyload.ACK_BIT == 1
        if ack:
            psn = packet_pyload.PSN
            self.printer(MEDIUM, "Got ack from host " + str(source_nic_id) + "\n" + str(packet_pyload))
            if psn != local_storage["next_psn"]:
                self.printer(MEDIUM, "ack DROPPED! duplicate ack")
                return
            if local_storage["waiting_packet"] is not None:
                expected_sid = local_storage["waiting_packet"].get_pyload().get_sid()
            ack_sid = packet_pyload.get_sid()
            if local_storage["waiting_packet"] is None or expected_sid != ack_sid:
                self.printer(MEDIUM, "ack DROPPED! unknown ack (probbably old)")
                return
                # dut_error(102, "arq got ack with wrong sid, ack: "
                #                "" + str(ack_sid) + " expected: " + str(expected_sid), name)
            local_storage["waiting_packet_vld"] = False
            local_storage["waiting_packet"] = None
            local_storage["packet_time_stamp"] = CURRENT_TIME()
            drop_objection(name)
        else:  # NOT ACK
            self.printer(MEDIUM, "Got new packet from: " + str(source_nic_id) + " with data, will send ack \n" + str(packet_pyload))
            new_ack = packet_pyload.create_ack(local_storage[NicId])
            wrapped_ack = ArqCommandResToSend(local_storage[NicId], source_nic_id, new_ack)
            local_storage["ack_waiting_list"].append(wrapped_ack)
            local_storage["ack_pending_vld"] = True
    else:
        dut_error(103, "arq got packet of type != Req/update", name)


def arq_default_final_handler(self, name, id, local_storage):
    pass


def gen_dest_host(except_id):
    # TODO: add support in auto recogise of num of hosts.
    passible_list = [1, 2, 3, 4, 5]
    passible_list.remove(except_id)
    return random.choice(passible_list)
