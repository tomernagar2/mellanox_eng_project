from generals import *
from globals import *


class RequestCommand(GeneralCommand):
    def __init__(self, target, source, **kwargs):
        self.target = target
        self.source = source
        super(RequestCommand, self).__init__(**kwargs)


class ResponseCommand(GeneralCommand):
    def __init__(self, target, source, **kwargs):
        self.target = target
        self.source = source
        super(ResponseCommand, self).__init__(**kwargs)

    @staticmethod
    def get_res_to_req(req):
        ##TODO: verifiy req is from type RequestCommand
        return ResponseCommand(target=req.source, source=req.target, sid=req.get_sid())


class UpdateCommand(GeneralCommand):
    def __init__(self, **kwargs):
        super(UpdateCommand, self).__init__(**kwargs)


# RequestCommand subTypes
class InitNewMessageCommand(RequestCommand):
    pass


class SendCommand(RequestCommand):
    pass


class OpenQpCommand(RequestCommand):
    def __init__(self, qp, **kwargs):
        super(OpenQpCommand, self).__init__(**kwargs)
        self.qp = qp


class CloseQpCommand(RequestCommand):
    def __init__(self, qp, **kwargs):
        super(CloseQpCommand, self).__init__(**kwargs)
        self.qp = qp


# ResponseCommand subTypes
class SendCompleteCommand(ResponseCommand):
    pass


class OpenQpCompleteCommand(ResponseCommand):
    @staticmethod
    def get_res_to_req(req):
        ##TODO: verifiy req is from type OpenQpCommand
        super_class = super(OpenQpCompleteCommand, OpenQpCompleteCommand).get_res_to_req(req)
        my_class = OpenQpCompleteCommand(target=super_class.target, source=super_class.source, super_class=req.get_sid())
        my_class.__dict__ = super_class.__dict__
        return my_class


class CloseQpCompleteCommand(ResponseCommand):
    pass


# UpdateCommand subTypes
class NewMessageArrivedCommand(UpdateCommand):
    pass
