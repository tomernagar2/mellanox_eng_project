from .packets import Packet
from configuration import arq_commands_packet_config as config_file


class ArqCommandPacket(Packet):
    def __init__(self, command_type, creator_id):
        super(ArqCommandPacket, self).__init__(size=0, packet_type=config_file.ArqCommandsPacketConfig.ArqPacketType)
        self.command_type = command_type
        self.creator_id = creator_id

    def get_command_type(self):
        return self.command_type


class ArqCommandReqToSend(ArqCommandPacket):
    def __init__(self, creator_id):
        super(ArqCommandReqToSend, self).__init__(command_type=config_file.ArqCommandsPacketConfig.ReqToSendCommandType,
                                                  creator_id=creator_id)


class ArqCommandResToSend(ArqCommandPacket):
    def __init__(self, creator_id, target_nic, pyload=None):
        super(ArqCommandResToSend, self).__init__(command_type=config_file.ArqCommandsPacketConfig.ResToSendCommandType,
                                                  creator_id=creator_id)
        self.pyload = pyload
        self.target_nic = target_nic

    def contain_data(self):
        return self.pyload is not None

    def get_pyload(self):
        return self.pyload

    def get_target(self):
        return self.target_nic


class ArqCommandRxUpdate(ArqCommandPacket):
    def __init__(self, creator_id, pyload=None):
        super(ArqCommandRxUpdate, self).__init__(command_type=config_file.ArqCommandsPacketConfig.RxUpdateCommandType,
                                                 creator_id=creator_id)
        self.pyload = pyload

    def get_pyload(self):
        return self.pyload

