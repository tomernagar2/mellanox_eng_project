from generals import *
from globals import *
import copy
from configuration import packets_config
config = packets_config.GeneralPacketConfig


class Packet(GeneralPacket):
    def __init__(self, size=None, packet_type=config.DefaultType, **kwargs):
        self.packet_type = packet_type
        super(Packet, self).__init__(size, **kwargs)


class TextPacket(Packet):
    def __init__(self, text, **kwargs):
        super(TextPacket, self).__init__(size=len(text), packet_type=config.GeneralTextPacketType, **kwargs)
        self.text = text

    def __str__(self):
        result = super(TextPacket, self).__str__()
        result += self.text + "\n"
        return result


class SAWArqPacket(Packet):
    def __init__(self, pyload=None, pyload_size=None, psn=0, source_nic_id=None, **kwargs):
        if pyload_size is None:
            pyload_size = len(pyload)
        super(SAWArqPacket, self).__init__(size=pyload_size, packet_type=config.ArqPacketType, **kwargs)
        self.pyload = pyload
        self.ACK_BIT = 0
        self.PSN = psn
        self.source_nic_id = source_nic_id

    def __str__(self):
        result = super(SAWArqPacket, self).__str__()
        result += "ACK_BIT: " + str(self.ACK_BIT) + "\n"
        result += "PSN: " + str(self.PSN) + "\n"
        result += "source_nic_id: " + str(self.source_nic_id) + "\n"
        result += "pyload: " + str(self.pyload) + "\n"
        return result

    def create_ack(self, nic_id):
        ack = SAWArqPacket(pyload="ack", pyload_size=config.AckPacketSize, gen_sid=False)
        ack.set_sid(self.get_sid())
        ack.ACK_BIT = 1
        ack.PSN = (self.PSN + 1) % 2
        ack.source_nic_id = nic_id
        return ack

    @staticmethod
    def gen_packet(psn, source_nic_id, **constrains):
        packet_min_size = 64
        packet_max_size = 1522
        if 'packet_min_size' in constrains:
            packet_min_size = constrains['packet_min_size']
        if 'packet_max_size' in constrains:
            packet_max_size = constrains['packet_max_size']
        pyload_size = random.randint(packet_min_size, packet_max_size)
        pyload = ''.join(chr(97 + random.randint(0, 25)) for i in range(pyload_size))
        return SAWArqPacket(pyload=pyload, pyload_size=pyload_size, psn=psn, source_nic_id=source_nic_id)


class EncapsulateDecapsulatePacket(GeneralPacket):
    pyload = None

    def encapsulate(self, pyload):
        self.size += pyload.size
        self.pyload = copy.deepcopy(pyload)

    def decapsulate(self, remove=True):
        result = copy.deepcopy(self.pyload)
        if remove:
            self.size -= self.pyload.size
            self.pyload = None
        return result


class LinkLayerLRH(EncapsulateDecapsulatePacket):
    LRHSize = 8  # bytes
    VL = None
    LVer = None
    SL = None
    LNH = None
    DLID = None
    SLI = None
    PktLen = None
    VCRC = None
    ICRC = None

    def __init__(self, VL=None, LVer=None, SL=None, LNH=None, DLID=None, SLI=None,
                 PktLen=None, VCRC=None, ICRC=None, **kwargs):
        self.VL = VL
        self.LVer = LVer
        self.SL = SL
        self.LNH = LNH
        self.DLID = DLID
        self.SLI = SLI
        self.PktLen = PktLen
        self.VCRC = VCRC
        self.ICRC = ICRC
        super(LinkLayerLRH, self).__init__(self.LRHSize, **kwargs)

    def __str__(self):
        titel = "\r\n--------------------------LinkLayerLRH--------size = "+str(self.size)+" bytes------"
        fields = "\r\nVL = " + str(self.VL) + \
                 "\r\nLVer = " + str(self.LVer) + \
                 "\r\nSL = " + str(self.SL) + \
                 "\r\nLNH = " + str(self.LNH) + \
                 "\r\nDLID = " + str(self.DLID) + \
                 "\r\nSLI = " + str(self.SLI) + \
                 "\r\nPktLen = " + str(self.PktLen) + \
                 "\r\nVCRC = " + str(self.VCRC) + \
                 "\r\nICRC = " + str(self.ICRC) + \
                 "\r\nPyload = " + str(self.pyload)
        footer = "\r\n----------------------------------------------------------------"

        return titel + fields + footer


class NetworkLayerGRH(EncapsulateDecapsulatePacket):
    GRHSize = 40  # bytes
    IPVer = None
    TClass = None
    FlowLable = None
    PayLen = None
    NxtHdr = None
    HopLmt = None
    SGID = None
    DGID = None

    def __init__(self, IPVer=None, TClass=None, FlowLable=None, PayLen=None, NxtHdr=None, HopLmt=None, SGID=None,
                 DGID=None, **kwargs):
        self.IPVer = IPVer
        self.TClass = TClass
        self.FlowLable = FlowLable
        self.PayLen = PayLen
        self.NxtHdr = NxtHdr
        self.HopLmt = HopLmt
        self.SGID = SGID
        self.DGID = DGID
        super(NetworkLayerGRH, self).__init__(self.GRHSize, **kwargs)

    def __str__(self):
        title = "\r\n--------------------------NetworkLayerGRH--------size = "+str(self.size)+" bytes--------"
        fields = "\r\nIPVer = " + str(self.IPVer) + \
                 "\r\nTClass = " + str(self.TClass) + \
                 "\r\nFlowLable = " + str(self.FlowLable) + \
                 "\r\nPayLen = " + str(self.PayLen) + \
                 "\r\nNxtHdr = " + str(self.NxtHdr) + \
                 "\r\nHopLmt = " + str(self.HopLmt) + \
                 "\r\nSGID = " + str(self.SGID) + \
                 "\r\nDGID = " + str(self.DGID) + \
                 "\r\nPyload = " + str(self.pyload)
        footer = "\r\n----------------------------------------------------------------"

        return title + fields + footer


class TransportLayerBTH(EncapsulateDecapsulatePacket):
    BTHSize = 12  # bytes
    OpCode = None
    SE = None
    M = None
    PadCount = None
    TVer = None
    PKey = None
    DestQP = None
    A = None
    PSN = None

    def __init__(self, OpCode=None, SE=None, M=None, PadCount=None, TVer=None, PKey=None, DestQP=None, A=None,
                 PSN=None, **kwargs):
        self.OpCode = OpCode
        self.SE = SE
        self.M = M
        self.PadCount = PadCount
        self.TVer = TVer
        self.PKey = PKey
        self.DestQP = DestQP
        self.A = A
        self.PSN = PSN
        super(TransportLayerBTH, self).__init__(self.BTHSize, **kwargs)

    def __str__(self):
        title = "\r\n--------------------------TransportLayerBTH-------size = "+str(self.size)+" bytes------"
        fields = "\r\nOpCode = " + str(self.OpCode) + \
                 "\r\nSE = " + str(self.SE) + \
                 "\r\nM = " + str(self.M) + \
                 "\r\nPadCount = " + str(self.PadCount) + \
                 "\r\nTVer = " + str(self.TVer) + \
                 "\r\nPKey = " + str(self.PKey) + \
                 "\r\nDestQP = " + str(self.DestQP) + \
                 "\r\nA = " + str(self.A) + \
                 "\r\nPSN  = " + str(self.PSN) + \
                 "\r\nPyload = " + str(self.pyload)
        footer = "\r\n----------------------------------------------------------------"

        return title + fields + footer
