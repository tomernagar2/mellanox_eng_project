from generals import *
from globals import *
from .packets import *
from .mng_commands import *
from configuration import mng_config


class MngUnit(GeneralUnit):
    """
        RxUnit object responsible on Rx flow.
    """
    def __init__(self, name, id, storage, config_file=mng_config.MngConfig()):
        """
        init new Rx unit object
        """

        handler = config_file.get_handler(mng_default_handler)
        init_handler = config_file.get_init_handler(mng_default_init_handler)
        finish_handler = config_file.get_final_handler(mng_final_handler)
        super(MngUnit, self).__init__(target=handler, target_init=init_handler, target_final=finish_handler, name=name, one_time=False, config_file=config_file, args=(self, name, id, storage,))


def mng_default_init_handler(self, name, id, local_storage):
    self.printer(HIGH, "I am mng #" + str(id) + " with name: " + str(name) + " started!")

    local_storage["FlowWaitingForCompletion"] = {}

    local_storage["SimFabricManager"] = FabricManager(self, local_storage[SimulationFabricName])
    local_storage["SimFabricManager"].\
        add_handler(OpenQpCommand.__name__, FabricManager.get_handler_wrapper(open_qp_handler,
                                                                  self, name, id, local_storage))

    local_storage["PciFabricManager"] = FabricManager(self, local_storage[PCIBusName])
    local_storage["PciFabricManager"]. \
        add_handler(OpenQpCompleteCommand.__name__, FabricManager.get_handler_wrapper(open_qp_complete_handler,
                                                                              self, name, id, local_storage))
    init_complete(name)


def mng_default_handler(self, name, id, local_storage):
    pci_fabric_manager = local_storage["PciFabricManager"]
    sim_fabric = local_storage["SimFabricManager"]
    sim_fabric.run()
    pci_fabric_manager.run()


def mng_final_handler(self, name, id, local_storage):
    self.printer(DEBUG, "Finalize")


def open_qp_handler(self, name, id, local_storage, packet):
    pci_fabric_manager = local_storage["PciFabricManager"]
    self.printer(HIGH, "got open QP request\n" + str(packet))
    local_storage["FlowWaitingForCompletion"][str(packet.get_sid())] = OpenQpCompleteCommand.get_res_to_req(packet)
    pci_fabric_manager.send(packet, dest_name="SharedMemory")
    self.printer(HIGH, "Qp request sent to SharedMemory")


def open_qp_complete_handler(self, name, id, local_storage, packet):
    sim_fabric = local_storage["SimFabricManager"]
    self.printer(HIGH, "got open QP Complete request\n" + str(packet))
    packet_sid = str(packet.get_sid())
    poped_req = local_storage["FlowWaitingForCompletion"][packet_sid]
    local_storage["FlowWaitingForCompletion"][packet_sid] = None

    sim_fabric.send(poped_req, dest_id=SimulationFabricId)
    self.printer(HIGH, "Qp opend res sent to SimulationFabric")
