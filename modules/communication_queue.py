from globals import *
from generals import *
from multiprocessing import Queue


class BusEntryManager(object):
    def __init__(self, bus_name, class_name=None, queue_class_name=None, externalBus=False):
        assert externalBus or class_name is not None, "NicConfig - InternalBus require class_name to init the Bus!" \
                                                      " class_name is None".format(self)
        self.bus_name = bus_name
        if not externalBus:
            self.class_name = class_name
        self.queue_class_name = queue_class_name
        self.externalBus = externalBus
        self.size = 0
        self.member_list = {}

    def set_size(self, size):
        self.size = size

    def create_bus(self):
        return self.class_name(num_of_types=self.size, custom_queue=self.queue_class_name, member_list=self.member_list)

    def add_member(self, name):
        self.member_list[name] = self.size
        messagef(DEBUG, "Member: " + str(name) + " got id: "+str(self.size), self.bus_name)
        self.size += 1
        return self.size - 1


class PersonalCustomQueueWrapper(object):
    def __init__(self, fabric, my_address):
        self.fabric = fabric
        self.my_address = my_address


class SelfWrapper(object):
    member_list = None

    def get_wrapper_for(self, **address_or_name):
        if address_or_name.get('name', None) is not None:
            address_or_name['address'] = self.member_list[address_or_name['name']]

        if address_or_name.get('address', None) is not None:
            return PersonalCustomQueueWrapper(self, address_or_name["address"])

        return None


class CommunicationQueue(SelfWrapper):
    """
    Queue class allowing reading of specific type from queue
    in other words allow all processes put pyload (with some type) and then read it by type.
    """

    qNum = 0
    _closed = False

    def __init__(self, num_of_types=1, custom_queue=None, member_list=None):
        # will rise exception in case of input error
        pos_int_check(num_of_types)
        self.qNum = num_of_types
        self.queueArray = []
        self.member_list = member_list
        self.packetOnTransmitArry = []  # TODO: unused - remove (I think).

        for _ in range(0, num_of_types):  # init all Queues
            if custom_queue is not None:
                self.queueArray.append(custom_queue())
            else:
                self.queueArray.append(Queue())

    @staticmethod
    def _get_name(queue_num):
        return "CommunicationQueue#" + str(queue_num)
    # def __getstate__(self):
    #     state = tuple(map(lambda q: q.__getstate__, self.queueArray))
    #     return state

    # def __setstate__(self, states):
    #     self.qNum = len(states)
    #     self.queueArray = []
    #     [self.queueArray.append(Queue().__setstate__(state)) for state in states]
    def get_address(self, name):
        assert self.member_list is not None, "CommunicationQueue {0!r} get_address while member_list is None!".format(self)
        result = self.member_list[name]
        assert result is not None, "CommunicationQueue {0!r} get_address name: {1!r} not exists!".format(self, name)
        return result

    def add_address(self, name, address):
        if self.member_list is None:
            self.member_list = {}
        self.member_list[name] = address

    def put(self, obj, type_number=0, block=True, timeout=None):
        assert not self._closed, "CommunicationQueue {0!r} has been closed".format(self)
        int_range_check(0, len(self.queueArray), type_number)
        raise_objection(CommunicationQueue._get_name(type_number))
        return self.queueArray[type_number].put(obj, block, timeout)

    def get_q_num(self):
        return self.qNum

    def get(self, type_number=0, block=True, timeout=None):
        int_range_check(0, len(self.queueArray), type_number)
        drop_objection(CommunicationQueue._get_name(type_number))
        return self.queueArray[type_number].get(block, timeout)

    def qsize(self, type_number=0):
        int_range_check(0, len(self.queueArray), type_number)
        return self.queueArray[type_number].qsize()

    def total_q_size(self):
        total = 0
        for q in self.queueArray:
            total += q.qsize()
        return total

    def empty(self, type_number=0):
        int_range_check(0, len(self.queueArray), type_number)
        return self.queueArray[type_number].empty()

    def full(self, type_number=0):
        int_range_check(0, len(self.queueArray), type_number)
        return self.queueArray[type_number].full()

    def get_nowait(self, type_number=0):
        return self.get(type_number=type_number, block=False, timeout=None)

    def put_nowait(self, obj, type_number=0):
        return self.put(obj, type_number=type_number, block=False, timeout=None)

    def close(self):
        for q in self.queueArray:
            q.close()
        self._closed = True

    def join_thread(self):
        assert self._closed, "CommunicationQueue {0!r} not closed".format(self)
        for q in self.queueArray:
            q.join_thread()

    def cancel_join_thread(self):
        for q in self.queueArray:
            q.join_thread()
