#!/usr/bin/python

from simulations.Ib_simulation import *
from modules.rx_unit import RxUnit


def main():
    # simulator configuration
    from sim_pack import sim_config
    sim_config.write()

    # simulation object
    simulation = GeneralIBSimulation()
    # NicConfig.MemberReqList = [
    #     NicConfig.MemberReqEntry('Rx', RxUnit, [NicConfig.InternalBusName, NicConfig.ExternalBusName, NicConfig.FabricName]),
    #     # MemberReqEntry('Tx', {InternalBusName, ExternalBusName, FabricName}),
    #     # MemberReqEntry('Arq', {InternalBusName, ExternalBusName}),
    #     # MemberReqEntry('SharedMemory', {ExternalBusName, PCIBusName}),
    #     # MemberReqEntry('ManagmentUnit', {PCIBusName, SimulationFabricName}),
    # ]

    # config simulation
    for _ in range(5):
        simulation.add_nic()

    simulation.start()

    # time.sleep(30)
    # simulator.stop()

    # Exit
    print('finished')


if __name__ == '__main__':
   main()
