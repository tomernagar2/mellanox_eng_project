from sim_pack import *
from generals import *
from modules import *
from generals import *
import random
import time


def test_thread(my_name):
    import time
    print(my_name, " - alive!")
    time.sleep(5)


def creshing_thread(my_name):
    import time
    time.sleep(15)
    print(my_name, " - About to cresh!..")
    t.v = 5


def dut_error_test(num_of_process=5):
    for i in range(num_of_process):
        GeneralThread(target=test_thread, name="thread#" + str(i), one_time=False, args=('t' + str(i),)).start()
    import time
    time.sleep(15)
    dut_error(123, "Dut error test info.....", "dut_error_test", raise_e=False)


def exception_error_test(num_of_process=5):
    for i in range(num_of_process):
        GeneralThread(target=test_thread, name="thread#" + str(i), one_time=False, args=('t' + str(i),)).start()
    GeneralThread(target=creshing_thread, name="thread#" + str(num_of_process), one_time=False,
                  args=('t' + str(num_of_process),)).start()


def propogation_queue_test(num_of_process=5):
    from sim_pack import sim_config
    sim_config.write()

    Fabric = PropagationQueue(num_of_types=num_of_process)

    def receive_packet(my_id, fabric):
        while not fabric.empty(my_id):
            packet = fabric.get(my_id)
            messagef(HIGH, str(my_id) + " <=  " + str(packet.pyload['from']) + " (" + str(
                packet.pyload['id']) + ") pyload: " + str(packet.pyload))
        messagef(HIGH, str(my_id) + " empty Q.")

    def test_fun(fabric, my_id):
        for _ in range(2):
            for index in range(num_of_process):
                receive_packet(my_id, fabric)
                rand_delay = random.randint(0, 3)
                time.sleep(rand_delay)
                message_id = random.randint(100, 1000)
                to_send = GeneralPacket(random.randint(10000, 600000))
                to_send.pyload = {
                    'type': "Message from: " + str(my_id),
                    'from': my_id,
                    'id': message_id,
                    'pyload': "Hii"
                }
                if index != my_id:
                    messagef(HIGH, str(my_id) + " => " + str(index) + " (" + str(message_id) + ") size: " + str(
                        to_send.size) + " pyload: " + str(to_send.pyload))
                    fabric.put(to_send, index)

    threads = []
    messagef(HIGH, "TEST_start")
    for i in range(num_of_process):
        thread = GeneralThread(target=test_fun, name=str(i), args=(Fabric, i,))
        thread.start()
        threads.append(thread)
    for i in range(num_of_process):
        threads[i].join()
    messagef(HIGH, "TEST_end")


def propogation_queue_test2(num_of_messages=5):
    from sim_pack import sim_config
    from configuration import nic_config
    sim_config.write()
    UnitInitReqEntry('Rx', RxUnit, [InternalBusName, ExternalBusName, FabricName]),

    Fabric = PropagationQueue(num_of_types=1)
    for i in range(num_of_messages):
        size = random.randint(1000, 1000000)
        packet = GeneralPacket(size)
        packet.pyload = {
            'data': "datatatata",
            'num': i
        }
        Fabric.put(packet, 0)
        while Fabric.empty(0):
            pass
        result = Fabric.get(0)
        messagef(HIGH, "packet finished at: " + str(CURRENT_TIME()))
        messagef(HIGH, "packet info: " + str(result.pyload) + " size: " + str(result.size))


def packets_basic_use_test():
    messagef(HIGH, "Testing packets!")
    pyload = TextPacket("Hello I am test pyload")

    messagef(HIGH, "Pyload: " + str(pyload))

    BTH = TransportLayerBTH(OpCode='Write', DestQP=44, PSN=3)
    messagef(HIGH, "TransportLayer created: " + str(BTH))

    GRH = NetworkLayerGRH(IPVer='Ipv4', PayLen=132, NxtHdr='you need to check')
    messagef(HIGH, "NetworkLayer created: " + str(GRH))

    LRH = LinkLayerLRH(VL=0, LVer=1, PktLen=100)
    messagef(HIGH, "LinkLayer created: " + str(LRH))

    messagef(HIGH, "Start Encapsulation:")
    BTH.encapsulate(pyload)

    messagef(HIGH, "pyload -> BTH:" + str(BTH))
    GRH.encapsulate(BTH)

    messagef(HIGH, "BTH -> GRH:" + str(GRH))
    LRH.encapsulate(GRH)

    messagef(HIGH, "GRH -> LRH:" + str(LRH))

    GRH = None
    BTH = None
    pyload = None

    messagef(HIGH, "Start Decapsulation:")

    GRH = LRH.decapsulate(True)
    messagef(HIGH, "LRH -> GRH:" + str(GRH))
    messagef(HIGH, "LRH after decapsulation: " + str(LRH))

    BTH = GRH.decapsulate(True)
    messagef(HIGH, "GRH -> BTH:" + str(BTH))
    messagef(HIGH, "GRH after decapsulation: " + str(GRH))

    pyload = BTH.decapsulate(True)
    messagef(HIGH, "BTH -> pyload:" + str(pyload))
    messagef(HIGH, "BTH after decapsulation: " + str(BTH))

    messagef(HIGH, "Test END")


def simulator_flow_test(num_of_nics=5):
    from simulations.Ib_simulation import GeneralIBSimulation as GeneralSimulation
    from configuration import nic_config as config

    def custom_nic_init_handler(self, nic_name, nic_id, local_storage):
        self.printer(HIGH, "I am nic #" + str(nic_id) + " with name: " + str(nic_name) + " started!")
        raise_objection(nic_name)
        init_complete(nic_name)
        local_storage['state'] = config.NicConfig.NIC_STATE_RUN
        local_storage['counter'] = 0

    def custom_nic_handler(self, nic_name, nic_id, local_storage):
        if local_storage['counter'] == 1:
            self.printer(HIGH, "I am nic #" + str(nic_id) + " and I have value 1")
        if local_storage['counter'] == 10000:
            drop_objection(nic_name)
            local_storage['counter'] += 1

    simulator = GeneralSimulation()
    config.NicConfig.set_custom_handler(custom_nic_handler)
    config.NicConfig.set_custom_init_handler(custom_nic_init_handler)

    for _ in range(num_of_nics):
        simulator.add_nic()

    simulator.start()
    return simulator


def simulator_fabric_packets_nic_test(num_of_nics=5):
    from simulations.Ib_simulation import GeneralIBSimulation as GeneralSimulation
    from configuration import nic_config as config
    from sim_pack import sim_config
    sim_config.write()
    NicConfig = config.NicConfig

    def nic_rx(self, nic_name, nic_id, fubric_obj, simulation_fabric_obj):
        fubric = fubric_obj.fabric
        simulation_fabric = simulation_fabric_obj.fabric
        address = simulation_fabric_obj.my_address
        if not simulation_fabric.empty(address):
            control_packet = simulation_fabric.get(nic_id)
            self.printer(HIGH, "Got Control packet! Q#"+str(address)+" :" + str(control_packet), str(nic_id))

    def custom_nic(self, nic_name, nic_id, local_storage):
        simulation_fabric = local_storage[SimulationFabricName]
        fabric = local_storage[FabricName]
        nic_state = local_storage.get('state', NicConfig.NIC_STATE_RESET)
        if nic_state == NicConfig.NIC_STATE_RESET:
            local_storage['state'] = NicConfig.NIC_STATE_INIT
            self.printer(HIGH, "Nic #" + str(nic_id) + " is up! nic_name(" + str(nic_name) + ")")
            return

        if nic_state == NicConfig.NIC_STATE_INIT:
            # init complete
            init_complete(nic_name, blocking=False)
            local_storage['state'] = NicConfig.NIC_STATE_RUN
            return

        if nic_state == NicConfig.NIC_STATE_RUN:
            nic_rx(self, nic_name, nic_id, fabric, simulation_fabric)
            # nic_tx(self, nic_name, nic_id, fabric, simulation_fabric, storage)
            # if sim_state[0] in [SIM_STATE_RUN] and storage['random_delay'] == 0:
            #     nic_tx_random(self, nic_name, nic_id, fabric, simulation_fabric, storage)

    config.NicConfig.set_custom_handler(custom_nic)
    simulator = GeneralSimulation()

    for _ in range(5):
        simulator.add_nic()

    simulator.start()
    # time.sleep(30)
    # simulator.stop()
    # simulator_fabric_packets_nic_test
    # from tests import goodTest as g
    # s = g.simulator_fabric_packets_nic_test()
    # s._simulation_fabric.put({'hiii Its me!!'},0)
    # Exit
    print('Simulator Started!')
    return simulator


def test_general_storage(num_of_tests=10):

    class TestObj(object):
        pass

    class TestElement(TestObj):
        pass
    for _ in range(num_of_tests):
        max_size = random.randint(10, 50)
        print("max_size: ",max_size)
        storage = GeneralStorage(TestObj, max_size)
        shadow_storage = []

        ## fill storage.
        for i in range(max_size):
            obj = TestElement()
            storage.add(obj)
            shadow_storage.append(obj)
            # print(i)

        if not storage.is_full():
            raise Exception("expect Fifo full while it not!")

        try:
            print("check overflow")
            obj = TestElement()
            storage.add(obj)
            raise Exception("Error - overflow not recognised!")
        except AssertionError as err:
            print("overflow recognised successfuly! ", err)

        #check random gets
        num_of_gets = random.randint(10, 50)
        print("num_pf_gets: ", num_of_gets)
        for i in range(max_size):
            address = random.randint(0, max_size)
            expect_error = address > (len(shadow_storage) -1) or address < 0
            try:
                get_from_storage = storage.get(address)
                if expect_error:
                    raise Exception("Error - index error didn't recognised! "
                                    "(index="+str(address)+" len = "+str(len(shadow_storage)))

                if get_from_storage != shadow_storage[address]:
                    print("get_from: ", get_from_storage)
                    print("shadow: ", shadow_storage[address])
                    raise Exception("Error - no match on get read ")

            except AssertionError as err:
                if expect_error:
                    print(" *index overflow* accure succesfuly! ", err)
                    pass
                else:
                    raise Exception("Error - unexpected index error "
                                    "(index=" + str(address) + " len = " + str(len(shadow_storage)))
            if storage.is_empty():
                raise Exception("Error - storage empty while has more packets.")

            pop_packet = storage.pop()
            array_pop = shadow_storage.pop(0)

            if pop_packet != array_pop:
                print("pop_packet: ", pop_packet)
                print("array_pop: ", array_pop)
                raise Exception("Error - no match on pop read ")
        try:
            print("check under_flow")
            storage.pop()
            raise Exception("Error - under_flow not recognised!")
        except AssertionError as err:
            print("underflow recognised successfuly! ", err)
    print("success!")


def test_basic_qp():
    qp_id = 5
    qp = BasicQP(qp_id, "data", "context")
    qp.init_wq(wq_id=0, qp_id=qp_id)
    qp.init_cq(cq_id=0, qp_id=qp_id)

    workQE = BasicWQE(data_size=100, targetQP=3, targetPC=1)
    complitionQE = BasicCQE(data_size=100, targetQP=3, targetPC=1, data="working!")
    messagef(DEBUG, workQE)
    print(complitionQE)
    workQ = qp.get_wq().add(workQE)
    compQ = qp.get_cq().add(complitionQE)
    print(qp)


def test_mng_shared_and_finalized(num_of_nics=5):
    from simulations.Ib_simulation import GeneralIBSimulation

    def mng_handler(self, name, id, local_storage):
        pci_fabric_manager = local_storage["PciFabricManager"]
        Packet = TextPacket("MngUnit Packet pyload")
        self.printer(DEBUG, "Sending Packet: \n" + str(Packet))
        pci_fabric_manager.send(Packet, 0)
        local_storage["PacketCounter"] = local_storage.get("PacketCounter", 0) + 1
        self.printer(DEBUG, "Packet Sent!")
        time.sleep(0.01)

    def mng_final_handler(self, name, id, local_storage):
        string = "\n"
        string += "---------------"+name+"---------------\n"
        string += "------------Simulator statistics-----------\n"
        string += "-------------------------------------------\n"
        string += "PacketCounter: " +str(local_storage.get("PacketCounter", 0)) + "\n"
        string += "-------------------------------------------\n"
        self.printer(DEBUG, string)

# simulator configuration
    from sim_pack import sim_config
    sim_config.write()
    mng_config.MngConfig.Custom_handler = staticmethod(mng_handler)
    mng_config.MngConfig.Custom_final_handler = staticmethod(mng_final_handler)
    mng_config.MngConfig.DisableFinalFunc = False

    # simulation object
    simulation = GeneralIBSimulation()
    simulation.set_sim_time(30)
    # config simulation
    for _ in range(num_of_nics):
        simulation.add_nic()

    simulation.start()


    # Exit
    print('finished')


if __name__ == "__main__":
    dut_error_test()
