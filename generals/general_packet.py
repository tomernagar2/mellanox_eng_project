from sim_pack import *
from .general_tools import SidClass


class GeneralPacket(SidClass):
    def __init__(self, size=None, **kwargs):
        super(GeneralPacket, self).__init__(**kwargs)
        self.size = size

    def get_size(self):
        return self.size

    def __str__(self):
        result = super(GeneralPacket, self).__str__()
        result += "Packet size: " + str(self.get_size()) + "\n"
        return result
