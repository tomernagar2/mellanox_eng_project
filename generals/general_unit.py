from sim_pack import *


class UnitInitReqEntry(object):
    def __init__(self, name, general_unit, req_kwords={}):
        self.ItemName = name
        self.Class = general_unit
        self.req_kwords = req_kwords


class GeneralUnit(GeneralThread):
    config = None

    def __init__(self, group=None, target=None, target_init=None, target_final=None, name=None, one_time=True, config_file=None, *args, **settings):
        self.config = config_file
        super(GeneralUnit, self).__init__(target=target, target_init=target_init, target_final=target_final, name=name, one_time=one_time, *args, **settings)

    buses_member_list = None
    buses_address = None

    def set_bus_address(self, bus_name, name, address):
        if self.buses_address is None:
            self.buses_address = {}
        if self.buses_address.get(bus_name, None) is None:
            self.buses_address[bus_name] = {}
        self.buses_address[bus_name][name] = address

    def get_bus_address(self, bus_name, name):
        result = self.buses_address[bus_name].get(name, None)
        assert result is not None, "GeneralUnit {0!r} get_bus_address() try to get Unknown Unit address".format(self)
        return result

    UnitsToInitList = None

    def add_unit_request(self, name, obj, req_kwords={}):
        if self.UnitsToInitList is None:
            self.UnitsToInitList = [UnitInitReqEntry(name, obj, req_kwords)]
        else:
            self.UnitsToInitList.append(UnitInitReqEntry(name, obj, req_kwords))

    def reset_unit_requests(self):
        self.UnitsToInitList = None

    def get_unit_requests(self):
        if self.UnitsToInitList is None:
            return []
        else:
            return self.UnitsToInitList