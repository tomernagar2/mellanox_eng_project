class GeneralUnitConfig(object):
    # params Unit required to run (need to pass inside 'storage')
    RequiredParamsList = None

    # if unit not need run Init set to True.
    DisableInitFunc = False

    # if unit not need run Final set to True.
    DisableFinalFunc = True

    # hold custom handlers for unit (else will run the default)
    Custom_handler = None
    Custom_init_handler = None
    Custom_final_handler = None

    def get_handler(self, default=None):
        result = default
        if self.Custom_handler is not None:
            result = self.Custom_handler
        return result

    def get_init_handler(self, default=None):
        result = default
        if self.DisableInitFunc:
            result = None
        elif self.Custom_init_handler is not None:
            result = self.Custom_init_handler
        return result

    def get_final_handler(self, default=None):
        result = default
        if self.DisableFinalFunc:
            result = None
        elif self.Custom_final_handler is not None:
            result = self.Custom_final_handler
        return result



