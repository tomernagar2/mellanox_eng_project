from sim_pack import *


class AutoObjPrinter(object):
    def __str__(self):
        result  = "\n{:_^63}\n".format('')
        result += "|{:^61}|\n".format("AutoPrinter Class: " + str(type(self).__name__))
        result += "{:_^63}\n".format('')
        for key, value in self.__dict__.items():
            row = "|{:^20}|{:^40}|\n"
            result += row.format(key, str(value))
        result += "{:_^63}\n".format('')
        return result


class CopyKwArgs(object):
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


class FabricManager(object):
    def __init__(self, unit, fabric_wrapper):
        self.printer = unit.printer
        self.fabric = fabric_wrapper.fabric
        self.my_address = fabric_wrapper.my_address
        self.type_handler_dict = {"Default": FabricManager.default_packet_handler}

    @staticmethod
    def get_handler_wrapper(func, *args, **kwargs):
        def wrapping_func(self, packet):
            kwargs["packet"] = packet
            return func(*args, **kwargs)
        return wrapping_func

    def add_handler(self, packet_type, handler):
        self.type_handler_dict[packet_type] = handler
        return self

    def run(self):
        if not self.fabric.empty(self.my_address):
            new_packet = self.fabric.get(self.my_address)
            packet_type = new_packet.__class__.__name__
            handler = self.type_handler_dict.get(packet_type, None)
            if handler is None:
                handler = self.type_handler_dict["Default"]
            handler(self, new_packet)

    def get(self):
        if self.fabric.empty(self.my_address):
            return None
        return self.fabric.get(self.my_address)

    def send(self, obj, dest_id=None, dest_name=None):
        if dest_name is not None:
            dest_id = self.fabric.member_list[dest_name]
        self.fabric.put(obj, dest_id)

    def default_packet_handler(self, new_packet):
        self.printer(MEDIUM, "Unknown Packet Arrived!\n" + str(new_packet), sub_module="FabricManager")


def gen_random_bool(true_priority=50):
    import random
    # TODO: add check that 0 =< Prio <=0
    return random.random()*100 <= true_priority


class SidClass(object):
    def __init__(self, sid=None, **kwargs):
        if sid is None:
            self._sid = get_new_sid()
        else:
            self._sid = sid
        self._sid_vld = True

    def get_sid(self):
        assert self._sid_vld, "SidClass {0!r} trying to get unset sid".format(self)
        return self._sid

    def sid_is_set(self):
        return self._sid_vld

    def set_sid(self, sid):
        self._sid_vld = True
        self._sid = sid

    def gen_sid(self, sid):
        self._sid_vld = True
        self._sid = get_new_sid()

    def copy_sid_from_other_packet(self, other):
        assert isinstance(other, SidClass), "copy_sid_from_other_packet() allowd onlt from other SidClass instance," \
                                            " while otherr type is: {0!r}".format(self)
        self.set_sid(other.get_sid())

    def __str__(self):
        result = "SidClass Sid: "
        if self.sid_is_set():
            result += str(self.get_sid()) + "\n"
        else:
            result += "Not set.\n"
        return result
