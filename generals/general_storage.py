# TODO: add support in multiStorage.
class GeneralStorage(object):
    _storage = None
    _max_size = None
    _elements_type = None

    def __init__(self, elements_type=None, max_size=None):
        self._max_size = max_size
        self._elements_type = elements_type
        self._storage = []

    def add(self, elem):
        assert not self.is_full(), "Storage.add {0!r} while storage full! ".format(self)
        assert self._elements_type is None or isinstance(elem, self._elements_type), "Storage.add {0!r} got element" \
                                                            " with type {1!r} while storage element_type " \
                                                            "is: {2!r}".format(self, type(elem), self._elements_type)

        self._storage.append(elem)

    def get(self, index=0):
        # only read WQE.
        assert index in range(len(self._storage)), "Storage.get {0!r} with index {1} " \
                                                   "while total size {2}".format(self, index, len(self._storage))
        return self._storage[index]

    def pop(self, index=0):
        # pop the WQE
        assert not self.is_empty(), "Storage.pop {0!r} pop while storage empty! ".format(self)
        assert index in range(len(self._storage)), "Storage.get {0!r} with index {1} " \
                                                   "while total size {2}".format(self, index, len(self._storage))
        return self._storage.pop(index)

    def is_full(self):
        return not (self._max_size is None or len(self._storage) < self._max_size)

    def is_empty(self):
        return len(self._storage) == 0

    def __str__(self):
        result =""
        for item in self._storage:
            result += str(item) + "\n"
        return result