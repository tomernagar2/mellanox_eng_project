from .general_storage import GeneralStorage
from .general_tools import *

class QpContext(AutoObjPrinter):
    pass


class QPData(AutoObjPrinter):
    pass


class CQ(GeneralStorage):
    _cq_id = None
    _qp_id = None

    def __init__(self, cq_id, qp_id, max_size=None):
        self._cq_id = cq_id
        self._qp_id = qp_id
        super(CQ, self).__init__(CQE, max_size)


class CQE(AutoObjPrinter):
    pass


class WQE(AutoObjPrinter):
    pass


class WQ(GeneralStorage):
    _wq_id = None
    _qp_id = None

    def __init__(self, wq_id, qp_id, max_size=None):
        self._wq_id = wq_id
        self._qp_id = qp_id
        super(WQ, self).__init__(WQE, max_size)


class QP(object):
    _qp_id = None
    _Wq_Array = None
    _Cq_Array = None

    def __init__(self, qp_id):
        self._qp_id = qp_id
        self._Wq_Array = {}
        self._Cq_Array = {}

    def get_qp_id(self):
        return self._qp_id

    def get_wq(self, wq_num='0'):
        return self._Wq_Array[str(wq_num)]

    def get_cq(self, cq_num='0'):
        return self._Cq_Array[str(cq_num)]

    def add_wq(self, wq, wq_id='0'):
        assert isinstance(wq, WQ), "QP {0!r} got wq with type {1!r} " \
                                            "no instance of WQ".format(self, type(wq))
        # TODO: add check that WQ not already in Array.
        self._Wq_Array[str(wq_id)] = wq

    def add_cq(self, cq, cq_id='0'):
        assert isinstance(cq, CQ), "QP {0!r} got cq with type {1!r} " \
                                            "no instance of CQ".format(self, type(cq))
        # TODO: add check that CQ not already in Array.
        self._Cq_Array[str(cq_id)] = cq

    def __str__(self):
        result = "\n{0:_^63}\n".format('')
        result += "{0:^61}\n".format("|QP " + str(self._qp_id) + " INFO|")
        result += "{0:_^63}\n".format('')
        result += "|{0:^61}|\n".format("WQs")
        result += "{0:_^63}\n".format('')
        for wq_id, wq in self._Wq_Array.items():
            result += "{0:_^63}\n".format('')
            result += "|{0:^61}|\n".format("WQ with id: " + str(wq_id))
            result += "{0:_^63}\n".format('')
            result += str(wq) + "\n"
        result += "{0:_^63}\n".format('')
        result += "|{0:^61}|\n".format("CQ")
        result += "{0:_^63}\n".format('')
        for cq_id, cq in self._Cq_Array.items():
            result += "{0:_^63}\n".format('')
            result += "|{0:^61}|\n".format("CQ with id: " + str(wq_id))
            result += "{0:_^63}\n".format('')
            result += str(cq) + "\n"
        result += "{0:_^63}\n".format('')
        return result