from generals import *
from globals import *


class NicConfig(GeneralUnitConfig):

    NIC_STATE_RESET = 0
    NIC_STATE_INIT = 1
    NIC_STATE_RUN = 2

    DisableInitFunc = False


    #      InternalBus         #
        # InternalBus = CommunicationQueue
        # InternalBusDefaultQueue = None
        # InternalBusName = "InternalBus"
        # UnitsOnInternalBus = ['Rx', 'Tx', 'Arq']

    #      ExternalBus         #
        # ExternalBus = CommunicationQueue
        # ExternalBusDefaultQueue = None
        # ExternalBusName = "ExternalBus"
        # UnitsOnInternalExternalBus = ['Rx', 'Tx', 'Arq', 'SharedMemory']

    #      PCIBus         #
        # PCIBus = CommunicationQueue
        # PCIBusDefaultQueue = None
        # PCIBusName = "PCIBus"
        # UnitsOnInternalPCIBus = ['ManagmentUnit', 'SharedMemory']

    #      Fabric         #

    @staticmethod
    def set_custom_handler(func):
        NicConfig.Custom_handler = staticmethod(func)

    @staticmethod
    def set_custom_init_handler(func):
        NicConfig.Custom_init_handler = staticmethod(func)
