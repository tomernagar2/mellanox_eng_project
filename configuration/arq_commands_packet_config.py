from .packets_config import GeneralPacketConfig


class ArqCommandsPacketConfig(object):
    ArqPacketType = GeneralPacketConfig.ArqCommandPacketType

    ##Internals Types
    ReqToSendCommandType = 0
    ResToSendCommandType = 1
    RxUpdateCommandType = 2

