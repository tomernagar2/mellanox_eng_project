from generals import *


class ArqConfig(GeneralUnitConfig):
    NEW_PACKET_P = 50
    ACK_PRIO = 50
    ARQ_TIME_OUT = datetime.timedelta(seconds=4)


    MAX_PACKET_SIZE = 500