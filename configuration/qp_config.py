from sim_pack.sim_config import SimConfig

class QPConfig(SimConfig):
    """

        configuration of simulation QP
    """

    # configuration ------------------------------------------START-------------------------------------------
    Q_PAIRS_NUMBER = 1  # number of Q_pairs in network

    MANUAL_QP_LIST = True  # don't gen QP list
    QP_CONFIG_LIST = [
        # if MANUAL_QP_LIST == True, set QPs pairs to here.
        # format: [{host1_num: {role, qp_num}, host2_num: {role, qp_num}},
        # {host1_num: {role, qp_num}, host2_num: {role, qp_num}}, ...]
        {0: {'rule': 'requester', 'qp_number': 0}, 1: {'rule': 'responder', 'qp_number': 1}},
        {0: {'rule': 'requester', 'qp_number': 1}, 2: {'rule': 'responder', 'qp_number': 0}},
        {1: {'rule': 'requester', 'qp_number': 0}, 2: {'rule': 'responder', 'qp_number': 1}},
        {2: {'rule': 'requester', 'qp_number': 2}, 0: {'rule': 'responder', 'qp_number': 2}},
     ]

    QP_LOOPBACK = False  # allow QPs sends and receive inside same host.


# configuration --------------------------------------------END-----------------------------------------

    INIT_DONE = False  # sign to __init__ not to regenerate parm more then ones

    @staticmethod
    def init_qp_params():
        # TODO: init all simulation params, like QPs pairs.

        if not QPConfig.MANUAL_QP_LIST:
            QPConfig.gen_qps()

        QPConfig.print_qp_configuration()
        #  init_qp_params()

    @staticmethod
    def gen_qps():
        # {host1_num: {role, qp_num}, host2_num: {role, qp_num}} when role = 'requester' / 'responder'
        # TODO: allow auto QP generation.
        pass

    @staticmethod
    def print_qp_configuration():
        print("qp configuration is:")
        print("-----------------------------------------------------------------------------------")
        print('|Pair #   |requester Address  |Responder address  |requester QP#  |Responder QP#  |')
        print("-----------------------------------------------------------------------------------")
        i = 0
        for conf in QPConfig.QP_CONFIG_LIST:
            requester_address = None
            responder_address = None
            requester_qp_num = None
            responder_qp_num = None

            for key in conf.keys():
                rule, qp_number = conf[key]['rule'], conf[key]['qp_number']
                if rule == 'requester':
                    requester_address = key
                    requester_qp_num = qp_number
                elif rule == 'responder':
                    responder_address = key
                    responder_qp_num = qp_number
                else:
                    raise ValueError("unknown qp role! should be 'requester'/responder only. rule value:", rule)
            print("|   ", i, "   |        ", requester_address, "  --------->  ", responder_address,
                  "           |     ", requester_qp_num, "   ---------->  ", responder_qp_num, "    |")
            i += 1
        print("-----------------------------------------------------------------------------------")

    def __init__(self, name="QPConfig", conf_id=0):
        SimConfig.__init__(self, name, conf_id)

        if not QPConfig.INIT_DONE:
            QPConfig.init_qp_params()
            QPConfig.INIT_DONE = True
        #  __init__()