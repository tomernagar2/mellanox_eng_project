from .sim_defines import *

import threading
import datetime

# global params

sim_state = [SIM_STATE_INIT]
_sim_state_waiting_list = []
_sim_req_init_state_list = []

_thread_list = []

int_debug = True
int_print_verbosity = {
    'default': LOW,  # required!
}

_last_sid = 100

def synchronized(func):
    """Make function synchronized - only one func in one time.
        NOT for classes!

        put @syncronized above the relevant function to wrap it.
    """
    func.__lock__ = threading.Lock()

    def synced_func(*args, **kws):
        with func.__lock__:
            return func(*args, **kws)

    return synced_func


def build_time_format(time_obj):
    """
    build string time format from datetime.datetime.now
    like: sec.milli.micro.
    :rtype: str
    :arg
     time_obj (datetime.datetime.now) : time to convert
    :return
     string : sec,milli,micro
    """
    total_micro_sec = time_obj.microseconds
    milli_sec = total_micro_sec // 1000
    micro_sec = total_micro_sec % 1000
    return str(time_obj.seconds)+","+str(milli_sec)+","+str(micro_sec)


SIM_START_TIME = datetime.datetime.now()
CURRENT_TIME = datetime.datetime.now


@synchronized
def simulation_printer(verb, message, time_obj, module_name='default', sub_module=None):
    """
    Print Simulation Data in multi threads environment synchronized.
    :param verb: message verbosity (HIGH, MEDIUM, LOW)
    :param message: the message to print
    :param time_obj: message time (sample from CURRENT_TIME())
    :param module_name: module requesting to print
    :param sub_module: sub module name to append to module file name
    :return: None
    """
    time_from_sim_start = build_time_format(time_obj - SIM_START_TIME)

    source_verb = int_print_verbosity.get(module_name)
    if source_verb is None:  # support in sub modules verbosity.
        matches = [value for key, value in int_print_verbosity.items() if key in module_name]
        if len(matches) != 0:
            source_verb = matches[0]
    if source_verb is None:
        source_verb = int_print_verbosity.get('default')
    if sub_module is not None:
        module_name = module_name + "." + str(sub_module)
    if (int_debug and verb == DEBUG) or verb <= source_verb:
        print("["+time_from_sim_start+"]<"+str(module_name)+">  " + str(message))


def messagef(verb, message, sub_module=None, module_name=None):
    """
    Print message to screen
    :param verb: verbosity of message
    :param message: string to print
    :param sub_module: sub module name to append to module file name
    :param module_name: module name when want to set menualy
    :return: None
    """
    ## STOP PRINT AFTER ERROR.
    if sim_state[0] == SIM_STATE_ERROR:
        return

    if module_name is None:
        import inspect
        module_file = inspect.stack()[1][1].split('/')[-1]
        module_name = module_file.split(".")[0]
    simulation_printer(verb, message, CURRENT_TIME(), module_name, sub_module)


@synchronized
def sim_state_operation(operation, unit_name):
    global sim_state, _sim_state_waiting_list, _sim_req_init_state_list
    if operation == 'add':
        if unit_name not in _sim_state_waiting_list:
            _sim_state_waiting_list.append(unit_name)
    if operation == 'remove' or operation == 'update':
        if operation == 'remove' and unit_name in _sim_state_waiting_list:
            _sim_state_waiting_list.remove(unit_name)
        if len(_sim_state_waiting_list) == 0 and sim_state[0] == SIM_STATE_GET_IDLE:
            sim_state[0] = SIM_STATE_IDLE
            messagef(LOW, "sim state changed to IDLE")
    if operation == 'add_init':
        if unit_name not in _sim_req_init_state_list:
            _sim_req_init_state_list.append(unit_name)
    if operation == 'remove_init' or operation == 'update_init':
        if operation == 'remove_init' and unit_name in _sim_req_init_state_list:
            _sim_req_init_state_list.remove(unit_name)
        if len(_sim_req_init_state_list) == 0 and sim_state[0] == SIM_STATE_GET_RUN:
            sim_state[0] = SIM_STATE_RUN
            messagef(LOW, "sim state changed to SIM_STATE_RUN")
        if sim_state[0] == SIM_STATE_GET_RUN:
            messagef(DEBUG, "DEBUG: item left to complete init: " + str(_sim_req_init_state_list))


def raise_objection(unit_name):
    messagef(MEDIUM, "raise objection: " + str(unit_name))
    sim_state_operation('add', unit_name)


def drop_objection(unit_name):
    messagef(MEDIUM, "drop objection: " + str(unit_name))
    sim_state_operation('remove', unit_name)


def get_objections():
    global _sim_state_waiting_list
    return _sim_state_waiting_list


def print_idle_objections():
    obj_list = get_objections()
    messagef(LOW, "OBJECT: " + str(obj_list))


def add_to_init_list(item_name):
    messagef(MEDIUM, "add_to_init_list: " + str(item_name))
    sim_state_operation('add_init', item_name)


def init_complete(item_name, blocking=True):
    global sim_state
    messagef(MEDIUM, "init_complete: " + str(item_name))
    sim_state_operation('remove_init', item_name)
    if blocking:
        wait_for(SIM_STATE_RUN, name=item_name)


def sim_state_get_run(unit_name=None):
    global sim_state
    if sim_state[0] != SIM_STATE_INIT:
        messagef(LOW, "ERROR!!! - sim_state_get_run while simulation NOT in SIM_STATE_INIT")
    else:
        sim_state[0] = SIM_STATE_GET_RUN
        messagef(LOW, "sim state changed to SIM_STATE_GET_RUN")
        sim_state_operation('update_init', unit_name)


def sim_state_get_idle(unit_name=None, fail_on_error=True):
    global sim_state
    if sim_state[0] != SIM_STATE_RUN:
        messagef(LOW, "ERROR!!! - sim state get idle while simulation NOT in RUN STATE")
        if fail_on_error:
            dut_error(GlobalDutError.IDLE_BEFORE_INIT, "Error sim_state_get_idle before init complete!", str(unit_name))
    else:
        sim_state[0] = SIM_STATE_GET_IDLE
        messagef(LOW, "sim state changed to GET_IDLE")
        sim_state_operation('update', unit_name)


def sim_state_stop():
    global sim_state
    if sim_state[0] != SIM_STATE_IDLE:
        messagef(LOW, "ERROR!!! - cant stop sim not in IDLE mode!")
    else:
        sim_state[0] = SIM_STATE_STOP
        messagef(LOW, "sim state changed to STOP")


def wait_for(value, wrapper=None, name="", blocking=True):
    """
    Wait for parm to change to "value", need to wrap the param with [param] = "wrapper" to get it ref
    default wrapper is [sim_state].
    :param value: dest value (value that will stop waiting)
    :param wrapper: list contain on element 0 (wrapper[0]) the param to compare with.
    :param name: allowed to add name to wait for printing resones
    :param blocking:    if blocking = True: block until == value,
                        if blocking == False: return with:
                            True if == value,
                            False if != value.
    """
    if wrapper is None:
        wrapper = sim_state
    messagef(HIGH, "wait_for("+name+") - start")
    sampling_fail = wrapper[0] != value
    while sampling_fail and blocking:
        sampling_fail = wrapper[0] != value
        pass
    messagef(HIGH, "wait_for("+name+") - end (arrived? = "+str(not sampling_fail)+")")
    return not sampling_fail


class GlobalDutError(object):
    STOP_SIM = 0
    IDLE_BEFORE_INIT = 1


def dut_error(num, message, item_name, wait_for_threads_to_end=False, raise_e=True):
    from .sim_generals import DutErrorExit
    global _thread_list
    line = "----------------------------------------"
    messagef(LOW, "\r\n"+line+"\r\nDUT_ERROR! num: " + str(num) + "\n\r source: " + str(item_name) + "\n\r info: " + str(message)+"\r\n"+line+"\r\n")
    thread_list_copy = _thread_list[:]
    messagef(LOW, "Units_to terminate are: " + str(thread_list_copy))
    sim_state[0] = SIM_STATE_ERROR
    for item in thread_list_copy:
        item.stop(blocking=wait_for_threads_to_end)
    if wait_for_threads_to_end:
        messagef(LOW, "Terminate complete!")
    if raise_e:
        raise DutErrorExit


def add_thread(tid):
    _thread_list.append(tid)


def remove_thread(tid):
    _thread_list.remove(tid)


@synchronized
def get_new_sid():
    global _last_sid
    _last_sid += 1
    return _last_sid


if __name__ == '__main__':
    """
        Test global module! - if module will run as main those functions will call and run the test.
    """
    import time

    def test_fun(source):
        raise_objection("by: " + str(source))
        init_complete(source)
        for _ in range(2):
            time.sleep(1)
            messagef(HIGH, "abcdefghijklmnopqrstuvw", source)

        if source == 9:
            dut_error(300, "I am dead!", "thread Id:" + str(source))
        drop_objection("by: " + str(source))

    threads = []
    messagef(HIGH, "TEST_start")
    for i in range(10):  # add units need to finish Init.
        _sim_req_init_state_list.append(i)
    _sim_req_init_state_list.append('main')
    raise_objection('main')
    for i in range(10):
        thread = threading.Thread(target=test_fun, args=(i,))
        thread.start()
        threads.append(thread)
    init_complete('main')

    wait_for(SIM_STATE_RUN)

    time.sleep(1)
    sim_state_get_idle()
    for i in range(10):
        threads[i].join()
    drop_objection('main')
    sim_state_stop()

    wait_for(SIM_STATE_STOP)

    messagef(HIGH, "TEST_end")
