from .sim_globals import *
from . import sim_globals
from threading import Thread


class DutErrorExit(BaseException):
    """ Request to exit from the interpreter. """

    def __init__(self, *args, **kwargs):  # real signature unknown
        pass

    code = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """exception code"""


class GeneralClass(object):
    name = None

    def _class_name(self):
        return type(self).__name__

    def set_verbosity(self, new_verb):
        sim_globals.int_print_verbosity[self._class_name] = new_verb

    def printer(self, verb, message, sub_module=None):
        # if sub_module is None and self.name is not None:
        #     sub_module = self.name
        #  messagef(verb, message, sub_module, module_name=self._class_name())
        messagef(verb, message, sub_module, module_name=self.name)


def wrapper(func, func_init, func_final, stop_flag, name='default_name', one_time=True):
    import threading

    def wrapped_func(*args, **kwargs):
        try:
            if func_init is not None:
                func_init(*args, **kwargs)
            while not stop_flag[0]:
                res = func(*args, **kwargs)
                if one_time or res is False:
                    break
                if sim_globals.sim_state[0] in [SIM_STATE_STOP, SIM_STATE_ERROR]:
                    break
            if func_final is not None:
                func_final(*args, **kwargs)

        except DutErrorExit:
            pass
        except Exception:
            print(str(name) + ' Got exception!')
            dut_error(0, "Thread with name: " + str(name) + " got Exception!", name, raise_e=False)
            raise
        finally:
            sim_globals.remove_thread(threading.current_thread())
            messagef(DEBUG, str(name) + ' -Terminated!', module_name=str(name))

    return wrapped_func


class GeneralThread(Thread, GeneralClass):
    """
    Hold Basic stracure of Thread
    """

    def __init__(self, group=None, target=None, target_init=None, target_final=None, name=None, one_time=True, *args, **settings):
        """This constructor should always be called with keyword arguments. Arguments are:

        *group* should be None; reserved for future extension when a ThreadGroup
        class is implemented.

        *target* is the callable object to be invoked by the run()
        method. Defaults to None, meaning nothing is called.

        *name* is the thread name. By default, a unique name is constructed of
        the form "Thread-N" where N is a small decimal number.

        *args* is the argument tuple for the target invocation. Defaults to ().

        *kwargs* is a dictionary of keyword arguments for the target
        invocation. Defaults to {}.

        If a subclass overrides the constructor, it must make sure to invoke
        the base class constructor (Thread.__init__()) before doing anything
        else to the thread.

        """
        assert target is not None, "No target to Thread! (unit_name= " + str(name) + ") - mandatory"
        self._stop_thread = [False]  ##TODO: replace with thread event.
        target = wrapper(target, target_init, target_final, self._stop_thread, name, one_time)
        Thread.__init__(self, group=group, target=target, name=name, *args, **settings)
        GeneralClass.__init__(self)

    def start(self):
        """
        Start thread,
        :return: None
        """

        super(GeneralThread, self).start()
        sim_globals.add_thread(self)
        messagef(DEBUG, "thread started! (" + self.name + ")")

    def stop(self, blocking=True):
        """
        Stops thread,
        :param blocking: True- will wait to thread to join, False- not wait for thread to stop
        :return: None
        """
        self._stop_thread[0] = True
        if blocking:
            self.join()


def sim_auto_stop(start_idle_time, max_stop_time):
    import time
    storage = {
        'start_idle_time': start_idle_time,
        'max_stop_time': max_stop_time
        }

    def init_auto_stop(storage):
        get_idle_dealy_sec = storage['start_idle_time']
        storage['get_idle_time'] = CURRENT_TIME() + datetime.timedelta(seconds=get_idle_dealy_sec)
        storage['in_get_idle'] = False

    def auto_stop_handler(storage):
        if not storage['in_get_idle'] and CURRENT_TIME() < storage['get_idle_time']:
            return
        elif not storage['in_get_idle'] and CURRENT_TIME() >= storage['get_idle_time']:
            sim_state_get_idle()
            storage['in_get_idle'] = True
            storage['exception_time'] = CURRENT_TIME() + datetime.timedelta(seconds=storage['max_stop_time'])
        elif not storage['in_get_idle']:
            return

        if not wait_for(SIM_STATE_IDLE, name='Simulator', blocking=False):
            time.sleep(4)
            print_idle_objections()
            if CURRENT_TIME() > storage['exception_time']:
                dut_error(GlobalDutError.STOP_SIM, "Stop sim - Wait too long (" + str(
                    storage['max_stop_time']) + " sec) for simulation to get Idle", "Simulator")
        else:
            sim_state_stop()
            wait_for(SIM_STATE_STOP)

    GeneralThread(target=auto_stop_handler, target_init=init_auto_stop, one_time=False, name='auto_stop', args=(storage,)).start()

