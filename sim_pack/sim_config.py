from . import sim_globals


class Config(object):
    NAME = None
    ID = None

    def __init__(self, name, conf_id):
        self.NAME = name
        self.ID = conf_id


class SimConfig(Config):
    SIM_TIME = 60.0  # SIM_TIME need to be >> SIM_INIT_DELAY
    SIM_TIME_MAX_WAIT = 120.0

    SIM_INIT_DELAY = 10.0
    # FabricName = 'Fabric'
    # SimulationFabricName = 'SimulationFabric'
    # BaseFabric = CommunicationQueue
    # DefaultFabric = PropogationQueue
    # DefaultSimulationFabric = None
    ## TODO: add here requerment to Nics Fabric and Simulation Fabric
    # @staticmethod
    # def init_simulation_params():
    #
    #     SimConfig.INIT_DONE = True
    #
    # def __init__(self, name="SimConfig", conf_id=0):
    #     Config.__init__(self, name, conf_id)
    #     if not SimConfig.INIT_DONE:
    #         SimConfig.sim_states = Array('i', self.HOST_NUMBER + 1)
    #         for i in range(0, len(SimConfig.sim_states)):
    #             SimConfig.sim_states[i] = SimStates.Init
    #         SimConfig.init_simulation_params()
    #     # __init__


class GlobalConfig(object):
    from sim_pack.sim_defines import HIGH, LOW, MEDIUM, STATISTICS

    DEBUG = False
    PRINT_VERBOSITY = {
        'default': HIGH,  # required!
        'sim_globals': LOW,  # required!
        'Nic#2Tx': HIGH,
            # Other Exampels.
        # 'sim_generals': HIGH,
        # 'GeneralIBSimulation': HIGH,
        # 'propagation_fabric': LOW,
        # 'Nic#3ManagmentUnit': HIGH,
        # 'SimulationController': HIGH,
        # 'SharedMemory': HIGH,
        # 'RxUnit': HIGH,
        # 'TxUnit': HIGH,
        # 'ArqUnit': HIGH,
        # 'MngUnit': HIGH,
    }

    @staticmethod
    def write(over_write=False):
        sim_globals.int_debug = GlobalConfig.DEBUG
        if over_write:
            sim_globals.int_print_verbosity = GlobalConfig.PRINT_VERBOSITY
        else:
            for unit_name in GlobalConfig.PRINT_VERBOSITY:
                sim_globals.int_print_verbosity[unit_name] = GlobalConfig.PRINT_VERBOSITY[unit_name]


def write(over_write=False):
    GlobalConfig.write(over_write)
