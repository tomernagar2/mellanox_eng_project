## PUT HERE ALL GLOBAL DEFINES

#simulation print defines
HIGH    = 10
MEDIUM  = 5
LOW     = 1
DEBUG   = 99999

#extended print defines
STATISTICS = 3

#simstate states
SIM_STATE_INIT      = 0
SIM_STATE_GET_RUN   = 1
SIM_STATE_RUN       = 2
SIM_STATE_GET_IDLE  = 3
SIM_STATE_IDLE      = 4
SIM_STATE_STOP      = 5
SIM_STATE_ERROR     = 99


