def pos_int_check(value):
    """
    check if number is positive int.else raise an ValueError exception
    """
    v_type = type(value)
    if int != v_type or value < 0:
        raise ValueError('Value not int or not positive integer.\n "value" = %s, and its type = %s.' % (value, v_type))


def int_range_check(lower_val, upper_val, value):
    """
    check if value is int and in lowerVal - upperVal range, raise ValueErorr else
    """
    l_type, u_type, v_type = type(lower_val), type(upper_val), type(value)
    if int != l_type or int != u_type or int != v_type:
        raise ValueError('"lowerVal" or "upperVal" or "value" not type %s .\n'
                         ' "lowerVal" = %s, type = %s.\n"upperVal" = %s, type = %s.\n"value" = %s, type = %s.'
                         % (int, lower_val, l_type, upper_val, u_type, value, v_type))

    if value not in range(lower_val, upper_val):
        raise ValueError('value (%s) not int range(%s, %s)' % (value, lower_val, upper_val))