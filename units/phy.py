# from generals import *
# from configuration.config import UnitName, SimStates, NicConfig
#
# class Phy(GeneralUnit):
#
#     def __init__(self):
#
#         # noinspection PyBroadException
#         try:  # python 3 and above
#             GeneralUnit.__init__(
#                 unitName=UnitName.Phy,
#                 unitHandler=self._phy_handler,
#             )
#         except Exception:  # backward computability
#             # python 2.7 and bellow
#             super(Phy, self).__init__(
#                 unitName=UnitName.Phy,
#                 unitHandler=self._phy_handler,
#             )
#             print("error")
#         self.config_obj = NicConfig()
#
#     def _phy_handler(self, *args, **kwargs):
#         print("phy start init...")
#         unit_state_index = self.config_obj.unit_state_index[UnitName.Phy]
#         self.config_obj.units_states[unit_state_index] = SimStates.Init
