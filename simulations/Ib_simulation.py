from generals import *
from globals import *
from modules import *
from sim_pack.sim_config import SimConfig
from sim_pack import sim_globals


class GeneralIBSimulation(GeneralClass):
    _nic_list = []
    _next_nic_id = 1  # 0 save for simulation controller
    _sim_time = SimConfig.SIM_TIME
    _sim_time_max_wait = SimConfig.SIM_TIME_MAX_WAIT
    _running = False
    _custom_fabric_obj = PropagationQueue
    _custom_simulation_fabric_obj = CommunicationQueue
    _fabric = None
    _simulation_fabric = None
    _sim_controller_obj = SimulationController
    sim_controller = None

    def _get_nic_id(self):
        result = self._next_nic_id
        self._next_nic_id += 1
        return result

    @staticmethod
    def _get_nic_name(nic_name, nic_id):
        if nic_name == 'auto':
            nic_name = "Nic#"+str(nic_id)
        return nic_name

    def add_nic(self, nic_name='auto', nic_obj=Nic):
        nic_id = self._get_nic_id()
        nic_name = self._get_nic_name(nic_name, nic_id)
        self._nic_list.append({
            'id'    : nic_id,
            'name'  : nic_name,
            'obj'   : nic_obj
        })

    def set_sim_time(self, sim_time = None, sim_max_wait = None):
        if self._running is True:
            self.printer(LOW, "Error! - set_sim_time() not allowed after simulation started")
            return
        if sim_time is not None:
            self._sim_time = sim_time
        if sim_max_wait is not None:
            self._sim_time_max_wait = sim_max_wait

    def set_custom_fabrics(self, fabric=None, simulation_fabric=None):
        if self._running is True:
            self.printer(LOW, "Error! - set_custom_fabrics() not allowed after simulation started")
            return
        if fabric is not None:
            self._custom_fabric_obj = fabric
        if simulation_fabric is not None:
            self._custom_simulation_fabric_obj = simulation_fabric

    def set_sim_controller(self, new_sim_controller):
        if self._running is True:
            self.printer(LOW, "Error! - set_sim_controller() not allowed after simulation started")
            return
        self._sim_controller_obj = new_sim_controller

    def start(self, auto_stop=True):
        if self._running is True:
            self.printer(LOW, "Error! - start() not allowed after simulation started")
            return
        sim_globals.sim_state[0] = SIM_STATE_INIT
        sim_globals.SIM_START_TIME = CURRENT_TIME()
        self.printer(MEDIUM, "start() starting simulation")
        add_to_init_list("Simulation")

        self._fabric = self._custom_fabric_obj(len(self._nic_list)+1)
        self._simulation_fabric = self._custom_simulation_fabric_obj(len(self._nic_list)+1)

        ##todo: add simulation_manager init.
        add_to_init_list(SimulationControllerName)
        self.sim_controller = self._sim_controller_obj(SimulationControllerName, SimulationControllerId, self._simulation_fabric)
        self.sim_controller.start()

        for nic in self._nic_list:
            add_to_init_list(nic['name'])
            nic_object = nic['obj'](nic['name'], nic['id'], self._fabric, self._simulation_fabric)
            nic_object.start()

        GeneralThread(target=self._auto_init, name='auto_init').start()

        init_complete("Simulation", blocking=False)
        self._running = True
        if auto_stop is True:
            self.stop()

    def stop(self):
        if self._running is not True:
            self.printer(LOW, "Error! - stop() not allowed before simulation started")
            return
        sim_auto_stop(self._sim_time, self._sim_time_max_wait)
        #GeneralThread(target=self._auto_stop, name='auto_stop', args=(self, delay_before_stop_sec, )).start()

    # def _auto_stop(self, sim_obj, delay_sec=None):
    #     # TODO: add fast terminate in case of sim_state ERROR.
    #     if delay_sec is None:
    #         delay_sec = self._sim_time
    #     time.sleep(delay_sec)
    #     sim_state_get_idle()
    #     exception_time = CURRENT_TIME() + datetime.timedelta(seconds=self._sim_time_max_wait)
    #     while not wait_for(SIM_STATE_IDLE, name='Simulator', blocking=False):
    #         time.sleep(4)
    #         print_idle_objections()
    #         if CURRENT_TIME() > exception_time:
    #             dut_error(GlobalDutError.STOP_SIM, "Stop sim - Wait too long ("+str(self._sim_time_max_wait)+" sec) for simulation to get Idle", "Simulator")
    #     sim_state_stop()
    #     wait_for(SIM_STATE_STOP)
    #     sim_obj._running = False

    def _auto_init(self, delay_sec=None):
        if delay_sec is None:
            delay_sec = SimConfig.SIM_INIT_DELAY
        time.sleep(delay_sec)
        sim_state_get_run()
